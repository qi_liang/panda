package org.panda.aop.proxy;

import org.panda.aop.util.InterfaceProxyFactoryUtil;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * jdk动态代理类
 * @author qi
 */
public class DynamicProxy implements InvocationHandler {


    public static <T> T newMapperProxy(Class<T> mammperInterface){
        ClassLoader classLoader = mammperInterface.getClassLoader();
        Class<?> [] interfaces = new Class[]{mammperInterface};
        InterfaceProxyFactoryUtil proxy = new InterfaceProxyFactoryUtil();
        return (T) Proxy.newProxyInstance(classLoader,interfaces,proxy);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = null;
        result = method.invoke(proxy,args);
        return result;
    }
}
