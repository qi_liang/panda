package org.panda.aop.proxy;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.List;

/**
 * @author qi
 * 代理管理器
 */
public class ProxyManger {

    /** 创建代理
     * Object o, Method method, Object[] objects, MethodProxy methodProxy
     * @param targetClass
     * @param proxyList
     * @param <T>
     * @return
     */
    public static <T> T createProxy(final Class targetClass, final List<Proxy> proxyList){

        return (T) Enhancer.create(targetClass, new MethodInterceptor() {
            @Override
            public Object intercept(Object targetObject, Method targetmMethod, Object[] methodParams, MethodProxy methodProxy) throws Throwable {

                return new ProxyChain(targetClass,targetObject,targetmMethod,methodProxy,methodParams,proxyList).doProxhChain();
            }
        });
    }
}
