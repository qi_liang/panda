package org.panda.aop.proxy;

import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @author qi
 * 代理链
 */
public class ProxyChain {

    /**
     * 目标class
     */
    private final Class<?> targetlass;
    /**
     *  目标对象
     */
    private final Object targetObject;
    /**
     * 目标方法
     */
    private final Method targetMethod;
    /**
     * 方法代理
     */
    private final MethodProxy methodProxy;
    /**
     * 方法参数
     */
    private final Object[] methodParams;
    /**
     *代理集合
     */
    private List<Proxy> proxyList ;
    /**
     * 代理索引
     */
    private int proxyIndex=0;

    /**
     * 构造方法
     */
    public ProxyChain(Class<?> targetlass,Object targetObject,Method targetMethod,MethodProxy methodProxy
            ,Object[] methodParams
            ,List<Proxy> proxyList){
        this.targetlass =targetlass;
        this.targetObject = targetObject;
        this.targetMethod = targetMethod;
        this.methodProxy = methodProxy;
        this.methodParams = methodParams;
        this.proxyList =proxyList;
    }


    /**
     * 返回方法参数
     * @return
     */
    public Object[] getMethodParams() {
        return methodParams;
    }

    /**
     * 返回目标class
     * @return
     */
    public Class getTargetlass() {
        return targetlass;
    }

    /**
     * 返回目标方法
     * @return
     */
    public Method getTargetMethod() {
        return targetMethod;
    }

    /**
     *  执行链表中的类代理方法并返回结果
     * @return
     * @throws Throwable
     */
    public Object doProxhChain()throws Throwable{
        Object methodResult;
        if (proxyIndex<proxyList.size()){
            methodResult = proxyList.get(proxyIndex++).doProxy(this);
        }else {
            methodResult = methodProxy.invokeSuper(targetObject,methodParams);
        }
        return methodResult;
    }
}
