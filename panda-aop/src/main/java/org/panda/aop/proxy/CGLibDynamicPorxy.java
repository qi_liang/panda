package org.panda.aop.proxy;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

/**
 * 采用cglib实现动态代理
 * @author qi
 */
public class CGLibDynamicPorxy implements MethodInterceptor {

    private static final Logger  LOGGER= LoggerFactory.getLogger(CGLibDynamicPorxy.class);

    private static CGLibDynamicPorxy instance = new CGLibDynamicPorxy();

    private CGLibDynamicPorxy(){

    }

    public static CGLibDynamicPorxy getInstance(){

        return instance;
    }

    public <T> T getProx(Class<T> clazz){

        return (T) Enhancer.create(clazz,this);
    }


    @Override
    public Object intercept(Object taget, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        before();
        Object result = methodProxy.invokeSuper(taget,objects);
        after();
        return result;
    }

    private void before(){
        System.out.println("Before");
    }

    private void after(){
        System.out.println("After");
    }
}
