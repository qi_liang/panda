package org.panda.aop.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

/**
 *  @author qi
 *  切面代理
 */
public abstract class AbstractAspectProxy implements Proxy{

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractAspectProxy.class);

    @Override
    public Object doProxy(ProxyChain proxyChain) throws Throwable {
    Object result = null;

    Class clazz = proxyChain.getTargetlass();
    Method method = proxyChain.getTargetMethod();
    Object[] parmas = proxyChain.getMethodParams();
    begin();
        if (intercept(clazz,method,parmas)){
            before(clazz,method,parmas);
            result = proxyChain.doProxhChain();
            after(clazz,method,parmas,result);
        }else{
            result = proxyChain.doProxhChain();
        }
        end();
        return result;
    }

    public  void begin(){

    }

    public boolean intercept(Class clazz,Method method,Object[] params)throws Throwable{
        return true;
    }

    /**
     *  抽象方法 -前置增强
     * @param clazz
     * @param method
     * @param params
     * @throws Throwable
     */
    public abstract void before(Class clazz,Method method,Object[] params)throws Throwable;

    /**
     *  抽象方法 -后置增强
     * @param clazz
     * @param method
     * @param params
     * @param result
     * @throws Throwable
     */
    public abstract void after(Class clazz,Method method,Object[] params,Object result)throws Throwable;

    public void error(Class clazz,Method method,Object[] params,Throwable e){

    }
    public void end(){

    }
}
