package org.panda.aop.annotation;

import java.lang.annotation.*;

/**
 * 切面注解
 * @author qi
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Aspect {

    Class<? extends Annotation> value();

}
