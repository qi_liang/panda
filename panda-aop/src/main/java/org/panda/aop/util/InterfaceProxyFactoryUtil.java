package org.panda.aop.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 为接口提供动态代理方式，可以尝试模拟 JPA
 * Created by qi_liang on 2018/4/23.
 * @author qi
 */
public class InterfaceProxyFactoryUtil implements InvocationHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(InterfaceProxyFactoryUtil.class);
    /**
     *
     * @param mammperInterface
     * @param <T>
     * @return
     */
    public static <T> T newMapperProxy(Class<T> mammperInterface){
        ClassLoader classLoader = mammperInterface.getClassLoader();
        Class<?> [] interfaces = new Class[]{mammperInterface};
        InterfaceProxyFactoryUtil proxy = new InterfaceProxyFactoryUtil();
        return (T) Proxy.newProxyInstance(classLoader,interfaces,proxy);
    }

    /**
     * 前置增强
     */
    private void before(){
        System.out.println("前置增强");
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = null;
        before();
        result = method.invoke(proxy,args);
        after();
       // System.out.println("=======接口调用方法开始======");
        //解析方法生成业务逻辑  start
       // System.out.println("method toGenericString:"+method.toGenericString());
       // System.out.println("method name:"+method.getName());
       // System.out.println("method args:"+args[0]);
        //解析方法生成业务逻辑  end
       // System.out.println("=======接口调用方法结束=======");
        return result;
    }

    /**
     * 后置增强
     */
    private void after(){
        System.out.println("后置增强");
    }
}
