package org.panda.aop.service;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 4:24 PM 2019/5/20
 */
public interface HelloService {

    String sayHello(String name);
}
