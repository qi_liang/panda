package org.panda.web.config;

/**
 *  配置类常量
 *  @author qi
 */
public class ConfigConstant {

    /**
     * 读取配置文件位置
     */
    public static final String CONFIG_FILE="smart.properties";

    public static final String JDBC_DRIVER = "jdbc.driver";

    public static final String JDBC_URL = "jdbc.url";

    public static final String JDBC_USER = "jdbc.user";

    public static final String JDBC_PASSWORD = "jdbc.password";

    public static final String APP_BASE_PACKAGE = "app.base_package";

    public static final String APP_JSP_PATH = "app.jsp_path";

    public static final String APP_ASSET_PATH = "app.asset_path";

    public static final String APP_UPLOAD_LIMIT = "app.upload_limit";

}
