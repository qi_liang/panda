package org.panda.web.mode;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 *  封装请求信息
 *  @author qi
 */
public class Request {

    /**
     *  请求方法
     */
    public String requestMetho;

    /**
     *  请求路径
     */
    private String requestPath;

    public Request(String requestMetho, String requestPath) {
        this.requestMetho = requestMetho;
        this.requestPath = requestPath;
    }

    public String getRequestMetho() {
        return requestMetho;
    }

    public void setRequestMetho(String requestMetho) {
        this.requestMetho = requestMetho;
    }

    public String getRequestPath() {
        return requestPath;
    }

    public void setRequestPath(String requestPath) {
        this.requestPath = requestPath;
    }

    @Override
    public boolean equals(Object object) {

        return EqualsBuilder.reflectionEquals(this,object);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
