package org.panda.web.mode;

/**
 * form 表单参数
 * @author qi
 */
public class FormParam {
    /**
     * 参数名称
     */
    private String fieldName;
    /**
     * 参数值
     */
    private String fieldValue;

    public FormParam(String fieldName, String fieldValue) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }
}
