package org.panda.web.mode;

import java.util.HashMap;
import java.util.Map;

/**
 * 放回视图对象
 * @author qi
 */
public class View {

    /**
     * 视图路径
     */
    private String path;

    /**
     * 模型数据
     */
    private Map<String,Object> model;

    public View(String path) {
        this.path = path;
        this.model = new HashMap<>();
    }

    public View addModel(String key,Object value){
        model.put(key,value);
        return this;
    }

    public String getPaht(){
        return path;
    }

    public Map<String, Object> getModel() {
        return model;
    }
}
