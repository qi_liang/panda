package org.panda.web.mode;

import org.panda.code.uitl.CastUtil;
import org.panda.code.uitl.CollectionUtil;
import org.panda.code.uitl.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  请求参数对象
 *  @author qi
 */
public class Param {

    private List<FormParam> formParamsList;

    private List<FileParam> fileParamList;

    public Param(List<FormParam> formParamsList) {
        this.formParamsList = formParamsList;
    }

    public Param(List<FormParam> formParamsList, List<FileParam> fileParamList) {
        this.formParamsList = formParamsList;
        this.fileParamList = fileParamList;
    }

    public Map<String,Object> getFieldMap(){
        Map<String,Object> fieldMap = new HashMap<>(formParamsList.size());
        if (CollectionUtil.isNotEmpty(formParamsList)){
            formParamsList.forEach((formParam)->{
                String fieldName = formParam.getFieldName();
                Object fieldValue = formParam.getFieldValue();
                if (fieldMap.containsKey(fieldName)){
                    fieldValue = fieldMap.get(fieldName)+ StringUtil.SEPARATOR+fieldValue;
                }
                fieldMap.put(fieldName,fieldValue);
            });
        }
        return fieldMap;
    }

    /**
     *  获取上传文件映射
     * @return
     */
    public Map<String,List<FileParam>> getFileMap(){
        Map<String,List<FileParam>> fileMap  = new HashMap<>(fileParamList.size());
        if (CollectionUtil.isNotEmpty(fileParamList)){
            fileParamList.forEach((fileParam)->{
                String fieldName = fileParam.getFieldName();
                List<FileParam> fileParamList;
                if (fileMap.containsKey(fieldName)){
                    fileParamList = fileMap.get(fieldName);
                }else{
                    fileParamList = new ArrayList<>();
                }
                fileParamList.add(fileParam);
                fileMap.put(fieldName,fileParamList);
            });
        }
        return fileMap;
    }

    /**
     * 获取所有上传文件
     * @param fieldName
     * @return
     */
    public List<FileParam> getFileList(String fieldName){

        return getFileMap().get(fieldName);
    }

    /**
     * 获取唯一上传文件
     * @param fieldName
     * @return
     */
    public FileParam getFile(String fieldName){

        List<FileParam> fileParamList = getFileList(fieldName);

        return fileParamList!=null&&fileParamList.size()>0?fileParamList.get(0):null;
    }

    private Map<String,Object> paramMap;

    /**
     *  获取上传参数映射
     * @param paramMap
     */
    public Param(Map<String, Object> paramMap) {
        this.paramMap = paramMap;
    }

    /**
     *  根据参数名获取long 型参数值
     * @param name
     * @return
     */
    public long getLong(String name){
        return CastUtil.castLong(paramMap.get(name));
    }

    /**
     * 根据参数获取String 参数
     * @param name
     * @return
     */
    public String getString(String name){

        return CastUtil.castString(paramMap.get(name));
    }

    /**
     *  根据参数获取double参数
     * @param name
     * @return
     */
    public double getDouble(String name){

        return CastUtil.castDouble(paramMap.get(name));
    }

    /**
     *  根据参数获取int类型参数
     * @param name
     * @return
     */
    public int getInt(String name){

        return CastUtil.castInt(paramMap.get(name));
    }

    /**
     *  根据参数获取boolean 类型参数
     * @param name
     * @return
     */
    public boolean getBoolean(String name){

        return CastUtil.castBoolean(paramMap.get(name));
    }

    public Map<String,Object> getMap(){

        return paramMap;
    }

    public boolean isEmpty(){
        return CollectionUtil.isEmpty(paramMap);
    }
}

