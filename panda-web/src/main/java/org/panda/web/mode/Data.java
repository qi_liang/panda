package org.panda.web.mode;

/**
 * 放回数据对象
 * @author qi
 */
public class Data {

    /**
     * 模型数据
     */
    private Object model;

    public Data(Object model) {
        this.model = model;
    }

    public Object getModel() {
        return model;
    }
}
