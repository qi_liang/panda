package org.panda.web.servlet;


import org.panda.web.helper.BeanHelper;
import org.panda.code.uitl.JsonUtil;
import org.panda.code.uitl.ReflectionUtil;
import org.panda.code.uitl.StringUtil;
import org.panda.web.helper.*;
import org.panda.web.mode.Data;
import org.panda.web.mode.Handler;
import org.panda.web.mode.Param;
import org.panda.web.mode.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * 统一请求拦截器
 *  @author qi
 */
@WebServlet(urlPatterns = "/*",loadOnStartup = 0)
public class DispatcherServlet extends HttpServlet{

    private final static Logger LOGGER = LoggerFactory.getLogger(DispatcherServlet.class);

    private final static String WEB_TEMPLATE_JSP = "jsp";

    private final static String WILDCARD = "*";

    private final static String REGISTRATION_DEFAULT = "default";

    private final static String FAVICON_ICO = "/favicon.ico";

    /**
     * 路径切割符号
     */
    public final static String PATH_CUT_SYMBOL = "/";


    @Override
    public void init(ServletConfig config) throws ServletException {
        //初始化容器
        HelperLoader.init();
        //获取ServletContext 对象(用于注册Servlet)
        ServletContext servletContext = config.getServletContext();

        ServletRegistration jspServlet = servletContext.getServletRegistration(WEB_TEMPLATE_JSP);
        jspServlet.addMapping(ConfigHelper.getAppJspPath()+WILDCARD);
        ServletRegistration defaultServlet = servletContext.getServletRegistration(REGISTRATION_DEFAULT);
        defaultServlet.addMapping(ConfigHelper.getAppAssetPath()+WILDCARD);
        //文件上传
        UploadHelper.init(servletContext);
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp){
        ServletHelper.init(req,resp);
        try {
            //获取请求的方法类型:(Post,Get...)
            String requestMethod = req.getMethod().toUpperCase();
            //获取请求的路径
            String requestPath = req.getPathInfo();
            //网站默认小图标
            if (FAVICON_ICO.equals(requestPath)){
                return;
            }
            //通过请求方法和路径获取请求的controller
            Handler handler = ControllerHelper.getHandler(requestMethod,requestPath);
            if(handler!=null){
                Class controllerClass = handler.getControllerClass();
                Object controllerBean = BeanHelper.getBean(controllerClass);
                //创建请求参数对象
                Param param;
                //判断是否文件上传
                if (UploadHelper.isMultipart(req)){
                    param = UploadHelper.createParam(req);
                }else {
                    param = RequestHelper.createParma(req);
                }
                Object result;
                Method actionMethod = handler.getActionMethod();
                if (param.isEmpty()) {
                    result = ReflectionUtil.invokeMethod(controllerBean,actionMethod);
                }else {
                    result = ReflectionUtil.invokeMethod(controllerBean, actionMethod, param);
                }
                //判断Action方法的返回值
                if (result instanceof View){
                    handleViewResult((View) result,req,resp);
                }else if (result instanceof Data){
                    handleDateResult((Data)result,req,resp);
                }

            }
        }catch (ServletException e){
            LOGGER.error("");
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }finally {
                ServletHelper.destory();
        }

    }


    /**
     * 处理视图类视图返回值
     */
    private void handleViewResult(View view,HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
        String path = view.getPaht();
        if (StringUtil.isNotNull(path)){
            if (path.startsWith(PATH_CUT_SYMBOL)) {
                response.sendRedirect(request.getContextPath() + path);
            } else {
                Map<String,Object> model = view.getModel();
                model.forEach((key,value)->{
                    request.setAttribute(key,value);
                });
                request.getRequestDispatcher(ConfigHelper.getAppJspPath()+path+".jsp").forward(request,response);

            }
        }
    }

    /**
     * 处理实体视图返回值
     */
    private void handleDateResult(Data data,HttpServletRequest request,HttpServletResponse response) throws IOException {

        //返回json数据
        Object model = data.getModel();
        if (model!=null){
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            PrintWriter writer = response.getWriter();
            String json = JsonUtil.toJson(model);
            writer.write(json);
            writer.flush();
            writer.close();
        }
    }
}
