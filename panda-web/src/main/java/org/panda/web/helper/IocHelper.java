package org.panda.web.helper;


import org.apache.commons.lang3.ArrayUtils;
import org.panda.code.uitl.CollectionUtil;
import org.panda.code.uitl.ReflectionUtil;
import org.panda.web.annotation.Inject;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * 依赖注入助手类
 * @author qi
 */
public class IocHelper {

    static {
        Map<Class, Object> beanMap = BeanHelper.getBeanMap();
        if (CollectionUtil.isNotEmpty(beanMap)) {
            beanMap.forEach((beanClass, beanInstance) -> {
                Field[] beanFields = beanClass.getDeclaredFields();
                if (ArrayUtils.isNotEmpty(beanFields)) {
                    for (Field beanField : beanFields) {
                        if (beanField.isAnnotationPresent(Inject.class)) {
                            Class beanFieldClass = beanField.getType();
                            Object beanFieldInstance = beanMap.get(beanFieldClass);
                            if (beanFieldInstance != null) {
                                ReflectionUtil.setField(beanInstance, beanField, beanFieldInstance);
                            }
                        }
                    }
                }
            });
        }

    }
}
