package org.panda.web.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 *  Servlet 助手类
 *  @author qi
 */
public class ServletHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServletHelper.class);

    private static final ThreadLocal<ServletHelper> SERVLET_HELPER_HOLEDER = new ThreadLocal<>();

    private HttpServletRequest request;

    private HttpServletResponse response;

    private ServletHelper(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    /**
     *  初始化
     */
    public static void init(HttpServletRequest request, HttpServletResponse response){

        SERVLET_HELPER_HOLEDER.set(new ServletHelper(request,response));
    }

    /**
     * 销毁
     */
    public static void destory(){
        SERVLET_HELPER_HOLEDER.remove();
    }

    /**
     *  获取request 对象
     */
    public static HttpServletRequest getRequest(){

      return SERVLET_HELPER_HOLEDER.get().request;
    }

    /**
     *  获取response 对象
     */
    public static HttpServletResponse getResponse(){

        return SERVLET_HELPER_HOLEDER.get().response;
    }

    /**
     * 获取session 对象
     */
    public static HttpSession getSession(){

        return getRequest().getSession();
    }

    /**
     * 获取 servletContext 对象
     */
    public static ServletContext getServletContext(){

        return getRequest().getServletContext();
    }

    /**
     *  将属性返回request中
     * @param key
     * @param value
     */
    public static void setRequestAttribute(String key,Object value){
        getRequest().setAttribute(key,value);
    }

    /**
     *  从request 获取属性
     * @param key
     * @param <T>
     * @return
     */
    public static <T> T getRequestAttribute(String key){

        return (T) getRequest().getRequestDispatcher(key);
    }

    /**
     *  从request 删除属性
     * @param key
     */
    public static void remoteRequestAttribute(String key){

        getRequest().removeAttribute(key);
    }

    /**
     * 发送重定向响应
     */
    public static void sendRedirect(String location){

        try {
            getResponse().sendRedirect(getRequest().getContextPath()+location);
        } catch (IOException e) {
            LOGGER.error("redirect failure",e);
          //  e.printStackTrace();
        }
    }

    /**
     *  将属性放入session中
     * @param key
     * @param value
     */
    public static void setSessionAttribute(String key,Object value){
        getSession().setAttribute(key,value);
    }

    /**
     *  从session 中获取属性
     * @param key
     * @param <T>
     * @return
     */
    public static <T> T getSessionAttribute(String key){
       return (T) getSession().getAttribute(key);
    }

    /**
     *  从sesion 中移除属性
     * @param key
     */
    public static void removeSessionAttribute(String key){

        getSession().removeAttribute(key);
    }

    /**
     *  使session 失效
     */
    public static void invaliateSession(){
        getRequest().getSession().invalidate();
    }
}
