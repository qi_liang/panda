package org.panda.web.helper;

import org.panda.code.uitl.PropsUtil;
import org.panda.web.config.ConfigConstant;

import java.util.Properties;

/**
 * 属性文件助手类
 * @author qi
 */
public class ConfigHelper {

    private static final Properties CONFIG_PROPS = PropsUtil.loadProps(ConfigConstant.CONFIG_FILE);

    /**
     * 获取数据库驱动
     */
    public static String getJdbcDriver() {

        return PropsUtil.getString(CONFIG_PROPS, "DbConfigConstant.JDBC_DRIVER");
    }

    /**
     * 获取数据库URL
     *
     * @return
     */
    public static String getJdbcUrl() {

        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_URL);
    }

    /**
     * 获取数据库用户
     *
     * @return
     */
    public static String getJdbcUser() {

        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_USER);
    }

    /**
     * 获取数据库密码
     */
    public static String getJdbcPassword() {

        return CONFIG_PROPS.getProperty(ConfigConstant.JDBC_PASSWORD);
    }

    /**
     * 获取包根路径
     */
    public static String getAppBasePackge() {

        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.APP_BASE_PACKAGE);
    }

    /**
     * 获取jsp路径
     */
    public static String getAppJspPath() {

        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.APP_JSP_PATH, "/WEB-INF/view/");
    }

    /**
     * 获取静态资源路径
     *
     * @return
     */
    public static String getAppAssetPath() {

        return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.APP_ASSET_PATH, "/asset/");
    }

    public static int getAppUplaodLimit() {

        return PropsUtil.getInt(CONFIG_PROPS, ConfigConstant.APP_UPLOAD_LIMIT, 10);
    }
}
