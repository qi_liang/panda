package org.panda.web.helper;

import org.panda.aop.annotation.Aspect;
import org.panda.aop.proxy.AbstractAspectProxy;
import org.panda.aop.proxy.Proxy;
import org.panda.aop.proxy.ProxyManger;
import org.panda.jdbc.proxy.TransactionProxy;
import org.panda.web.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.util.*;

/**
 * 方法拦截助手类
 * @author qi
 */
public final class AopHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(AopHelper.class);

    static {
        try {
            Map<Class, Set<Class>> proxyMap = createProxyMap();
            Map<Class, List<Proxy>> targetMap = createTargetMap(proxyMap);
            targetMap.forEach((targetClass, proxyList) -> {
                Object proxy = ProxyManger.createProxy(targetClass, proxyList);
                BeanHelper.setBean(targetClass, proxy);
            });
        } catch (Exception e) {
            LOGGER.error("aop failure", e);
        }

    }

    /**
     * 创建 annotation 和 带有annotation的class集合
     *
     * @return
     */
    private static Map<Class, Set<Class>> createProxyMap() {
        //初始化结果集合
        Map<Class, Set<Class>> proxyMap = new HashMap<>(16);
//        //代理class 集合
//        Set<Class> proxyClassSet = ClassHelper.getClasswetBySuper(AspectProxy.class);
//        //代理集合遍历
//        proxyClassSet.forEach((proxyClass)->{
//            //判断是否加包含注解Aspect
//           if (proxyClass.isAnnotationPresent(Aspect.class)){
//               //把注解对象取出来
//               Aspect aspect = (Aspect) proxyClass.getAnnotation(Aspect.class);
//               //注解对象中的vale集合
//               Set<Class> targetClassSet = createTargetClassSet(aspect);
//               //加入结果集
//               proxyMap.put(proxyClass,targetClassSet);
//           }
//        });
        addAspectProxy(proxyMap);
        addTransactionProxy(proxyMap);
        return proxyMap;
    }

    /**
     * 返回特定注解的class
     *
     * @param aspect
     * @return
     */
    public static Set<Class> createTargetClassSet(Aspect aspect) {

        Set<Class> targetClassSet = new HashSet<>();
        Class<? extends Annotation> annotation = aspect.value();
        if (annotation != null && !annotation.equals(Aspect.class)) {
            targetClassSet.addAll(ClassHelper.getClassSetByAnnotation(annotation));
        }
        return targetClassSet;
    }

    private static Map<Class, List<Proxy>> createTargetMap(Map<Class, Set<Class>> proxyMap) {
        Map<Class, List<Proxy>> targetMap = new HashMap<>(proxyMap.size());
        proxyMap.forEach((proxyClass, targetClassSet) -> {
            targetClassSet.forEach((targetClass) -> {
                try {
                    Proxy proxy = (Proxy) proxyClass.newInstance();
                    if (targetMap.containsKey(targetClass)) {
                        targetMap.get(targetClass).add(proxy);
                    } else {
                        List<Proxy> proxyList = new ArrayList<>();
                        proxyList.add(proxy);
                        targetMap.put(targetClass, proxyList);
                    }
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            });
        });
        return targetMap;
    }

    /**
     * 加入切面代理
     *
     * @param proxyMap
     */
    private static void addAspectProxy(Map<Class, Set<Class>> proxyMap) {
        Set<Class> proxyClassSet = ClassHelper.getClassSetBySuper(AbstractAspectProxy.class);

        proxyClassSet.forEach((proxyClass) -> {
            if (proxyClass.isAnnotationPresent(Aspect.class)) {
                Aspect aspect = (Aspect) proxyClass.getAnnotation(Aspect.class);
                Set<Class> targetClassSet = createTargetClassSet(aspect);
                proxyMap.put(proxyClass, targetClassSet);
            }
        });
    }

    /**
     *  加入事务代理
     * @param proxyMap
     */
    private static void addTransactionProxy(Map<Class, Set<Class>> proxyMap) {
        Set<Class> serviceClassSet = ClassHelper.getClassSetByAnnotation(Service.class);
        proxyMap.put(TransactionProxy.class, serviceClassSet);
    }
}
