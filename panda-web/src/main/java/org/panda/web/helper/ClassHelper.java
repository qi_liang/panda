package org.panda.web.helper;

import org.panda.beans.util.ClassUtil;
import org.panda.web.annotation.Controller;
import org.panda.web.annotation.Service;

import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.Set;

/**
 * @author qi
 */
public class ClassHelper {

    private static final Set<Class> CLASS_SET;

    static {
        String basePath = ConfigHelper.getAppBasePackge();
        CLASS_SET = ClassUtil.getClassSet(basePath);
    }

    /**
     * 获取全部class
     *
     * @return
     */
    public static Set<Class> getClassSet() {

        return CLASS_SET;
    }

    /**
     *  获取特定包下的class
     * @param basePath
     * @return
     */
    public static Set<Class> getPackgeAllClassSet(String basePath){
        Set<Class> classSet =  ClassUtil.getClassSet(basePath);;
        return classSet;
    }


    /**
     * 获取带有注解 service的class
     *
     * @return
     */
    public static Set<Class> getServiceClassSet() {
        Set<Class> classSet = new HashSet<>();
        CLASS_SET.forEach(clazz -> {
            if (clazz.isAnnotationPresent(Service.class)) {
                classSet.add(clazz);
            }
        });
        return classSet;
    }

    /**
     * 获取带有注解 controller的class
     *
     * @return
     */
    public static Set<Class> getControllerClassSet() {
        Set<Class> classSet = new HashSet<>();
        CLASS_SET.forEach(clazz -> {
            if (clazz.isAnnotationPresent(Controller.class)) {
                classSet.add(clazz);
            }
        });
        return classSet;
    }

    /**
     * 获取包下所有的class(包含 service,controller)
     *
     * @return
     */
    public static Set<Class> getBeanClassSet() {
        Set<Class> beanClassSet = new HashSet<>();
        beanClassSet.addAll(getServiceClassSet());
        beanClassSet.addAll(getControllerClassSet());
        return beanClassSet;
    }

    /**
     * 获取应用包名下的某父类（或接口）的所有子类（或实现类）
     */
    public static Set<Class> getClassSetBySuper(Class superClass) {
        Set<Class> classSet = new HashSet<>();
        CLASS_SET.forEach((clazz) -> {
            if (superClass.isAssignableFrom(clazz) && (!superClass.equals(clazz))) {
                classSet.add(clazz);
            }
        });
        return classSet;
    }

    /**
     * 获取应用包下面带某些注解的所有类
     */
    public static Set<Class> getClassSetByAnnotation(Class<? extends Annotation> annotationlass) {
        Set<Class> classSet = new HashSet<>();
        CLASS_SET.forEach((clazz) -> {
            if (clazz.isAnnotationPresent(annotationlass)) {
                classSet.add(clazz);
            }
        });
        return classSet;
    }

}
