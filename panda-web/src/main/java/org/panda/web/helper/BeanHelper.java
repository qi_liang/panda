package org.panda.web.helper;

import org.panda.code.uitl.ReflectionUtil;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Bean助手类
 * @author qi
 */
public class BeanHelper {

    private static Map<Class, Object> BEAN_MAP = new HashMap<>();

    static {
        Set<Class> beanClassSet = ClassHelper.getBeanClassSet();

        beanClassSet.forEach(clazz -> {
            Object obj = ReflectionUtil.newInstance(clazz);
            BEAN_MAP.put(clazz, obj);
        });
    }

    /**
     * 获取 Bean 映射
     *
     * @return
     */
    public static Map<Class, Object> getBeanMap() {
        return BEAN_MAP;
    }

    /**
     * 获取 Bean 实例
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T getBean(Class<T> clazz) {

        if (!BEAN_MAP.containsKey(clazz)) {
            throw new RuntimeException("can not get bean by class" + clazz);
        }
        return (T) BEAN_MAP.get(clazz);
    }

    /**
     * 设置Bean实例
     */
    public static void setBean(Class clazz, Object obj) {
        BEAN_MAP.put(clazz, obj);
    }
}
