package org.panda.web.helper;

import org.panda.code.uitl.ArrayUtil;
import org.panda.code.uitl.CollectionUtil;
import org.panda.code.uitl.StringUtil;
import org.panda.web.annotation.Action;
import org.panda.web.mode.Handler;
import org.panda.web.mode.Request;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 控制器助手
 * @author qi
 */
public final class ControllerHelper {

    /**
     * 用于存放请求与处理起的映射关系()
     */
    private static final Map<Request, Handler> ACTION_MAP = new HashMap<>();

    static {
        Set<Class> controllerClassSet = ClassHelper.getControllerClassSet();
        if (CollectionUtil.isNotEmpty(controllerClassSet)) {
            controllerClassSet.forEach((controllerClass) -> {
                //获取控制类中的所有方法
                Method[] methods = controllerClass.getMethods();
                if (ArrayUtil.isNotEmpty(methods)) {
                    for (Method method : methods) {
                        if (method.isAnnotationPresent(Action.class)) {
                            Action action = method.getAnnotation(Action.class);
                            String mapping = action.value();
                            String methodType = action.methodType();
                            //验证url 映射规则
//                            if (mapping.matches("\\w+:/\\w*")){
                            if (StringUtil.isNotNull(mapping)) {
                                String requestMethod = methodType;
                                String requestPath = mapping;
                                Request request = new Request(requestMethod, requestPath);
                                Handler handler = new Handler(controllerClass, method);
                                ACTION_MAP.put(request, handler);
                            }
                        }
                    }
                }
            });
        }
    }

    /**
     * 获取 Handler
     *
     * @return
     */
    public static Handler getHandler(String requestMethod, String requestPath) {
        Request request = new Request(requestMethod, requestPath);
        Handler handler = ACTION_MAP.get(request);
        return handler;
    }

}
