package org.panda.web.helper;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.panda.beans.util.ClassUtil;

/**
 * @author qi
 */
public class HelperLoader {

    public static void init() {
        /**
         *  aopHelper 需要在iocHelper前加载
         */
        Class[] classList = {ClassHelper.class, BeanHandler.class, AopHelper.class, IocHelper.class, ControllerHelper.class};
        for (Class clazz : classList) {
            ClassUtil.loadClass(clazz.getName(), true);
        }
    }
}
