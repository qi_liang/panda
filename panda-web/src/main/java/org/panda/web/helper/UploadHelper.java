package org.panda.web.helper;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.panda.code.uitl.CollectionUtil;
import org.panda.code.uitl.StreamUtil;
import org.panda.code.uitl.StringUtil;
import org.panda.code.uitl.file.FileUtil;
import org.panda.web.mode.FileParam;
import org.panda.web.mode.FormParam;
import org.panda.web.mode.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 文件上传助手类
 * @author qi
 */
public class UploadHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(UploadHelper.class);

    /**
     * apache Commons fileUpload 提供的上传对象
     */
    private static ServletFileUpload servletFileUpload;

    /**
     * 初始化
     *
     * @param servletContext
     */
    public static void init(ServletContext servletContext) {
        File repository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
        servletFileUpload = new ServletFileUpload(new DiskFileItemFactory(DiskFileItemFactory.DEFAULT_SIZE_THRESHOLD, repository));
        //上传文件的最大值
        int uploadLimit = ConfigHelper.getAppUplaodLimit();
        if (uploadLimit != 0) {
            servletFileUpload.setFileSizeMax(uploadLimit * 1024 * 1024);
        }
    }

    /**
     * 判断请求是否为multipart类型
     *
     * @param request
     * @return
     */
    public static boolean isMultipart(HttpServletRequest request) {

        return ServletFileUpload.isMultipartContent(request);
    }

    /**
     * 创建请求对象
     */

    public static Param createParam(HttpServletRequest request) throws IOException {

        List<FormParam> formParamList = new ArrayList<>();
        List<FileParam> fileParamList = new ArrayList<>();

        try {
            Map<String, List<FileItem>> fileItemListMap = servletFileUpload.parseParameterMap(request);
            if (CollectionUtil.isNotEmpty(fileItemListMap)) {
                fileItemListMap.forEach((fieldName, fileItemList) -> {
                    fileItemList.forEach((fileItem) -> {
                        if (fileItem.isFormField()) {
                            try {
                                String fieldValue = fileItem.getString("UTF-8");
                                formParamList.add(new FormParam(fieldName, fieldValue));
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }

                        } else {
                            try {
                                String fileName = FileUtil.getRealFileName(new String(fileItem.getName().getBytes(), "UTF-8"));
                                if (StringUtil.isNotNull(fileName)) {
                                    if (StringUtil.isNotNull(fileName)) {
                                        long fileSize = fileItem.getSize();
                                        String contentType = fileItem.getContentType();
                                        InputStream inputStream = fileItem.getInputStream();
                                        fileParamList.add(new FileParam(fieldName, fileName, fileSize, contentType, inputStream));
                                    }
                                }
                            } catch (UnsupportedEncodingException e) {
                                LOGGER.error("encoding error", e);
                                throw new RuntimeException(e);
                                // e.printStackTrace();
                            } catch (IOException e) {
                                LOGGER.error("IO error", e);
                                //  e.printStackTrace();
                            }
                        }
                    });
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Param(formParamList, fileParamList);

    }

    /**
     * 上传文件
     *
     * @param basePath
     * @param fileParam
     */
    public static void uploadFile(String basePath, FileParam fileParam) {
        try {
            if (fileParam != null) {
                String filePath = basePath + fileParam.getFileName();
                FileUtil.createFile(filePath);
                InputStream inputStream = new BufferedInputStream(fileParam.getInputStream());
                OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(filePath));
                StreamUtil.copyStream(inputStream, outputStream);
            }
        } catch (FileNotFoundException e) {
            LOGGER.error("upload file failure", e);
            throw new RuntimeException(e);
            // e.printStackTrace();
        }

    }

    /**
     * 批量上传文件
     *
     * @param basePath
     * @param fileParamList
     */
    public static void uploadFile(String basePath, List<FileParam> fileParamList) {
        if (CollectionUtil.isNotEmpty(fileParamList)) {
            fileParamList.forEach((fileParam) -> {
                uploadFile(basePath, fileParam);
            });
        }

    }
}
