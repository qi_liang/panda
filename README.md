熊猫框架
====================

## 开发环境
1. mavne3 
2. jdk1.8

## 目录说明
|目录|说明|
|:---:|:---:|
|panda-all|全部包|
|panda-aop|AOP相关工具类|
|panda-beans|bean相关工具类
|panda-code|核心工具类|
|panda-jdbc|数据库相关工具类|
|panda-web|web相关工具类|
   




