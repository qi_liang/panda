package org.panda.jdbc;

import org.panda.beans.util.BeanUtil;
import org.panda.code.uitl.ReflectionUtil;

import org.panda.jdbc.sql.BeanSqlUtil;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;

/**
 *  @author qi
 *  结果集处理类
 */
public class BeanHander implements ResultSetHandler {

	private Class<?> clazz;

	public BeanHander(Class<?> clazz) {
		this.clazz = clazz;
	}

	@Override
	public Object handler(ResultSet rs) {
		try {
			Object bean = BeanUtil.instantiateClass(clazz);
			List<Field> fieldList = BeanUtil.getAllField(clazz);
			List<Method> methodList =BeanUtil.getAllMethods(clazz);
		 	for (Field f:fieldList){
		 		String fieldName = f.getName();
		 		String columnName =  BeanSqlUtil.caseToHump(fieldName);
		 		try {
					Object columnData = rs.getObject(columnName);
					ReflectionUtil.setField(bean,f,columnData);
				}catch (SQLException e){
		 			continue;
				}

		 }
		return bean;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

}
