package org.panda.jdbc;

import org.panda.jdbc.helper.DbConfigInterface;

/**
 * @author qi
 */
public class JdbcLoad {

    private String driver = null;

    private String url = null;

    private String username = null;

    private String password = null;

    public JdbcLoad(String driver, String url, String username, String password) {
        this.driver = driver;
        this.url = url;
        this.username = username;
        this.password = password;
        loadDriver();
    }

    public JdbcLoad(DbConfigInterface dbConfigInterface){
        this.driver = dbConfigInterface.getJdbcDriver();
        this.url = dbConfigInterface.getJdbcUrl();
        this.username =dbConfigInterface.getJdbcUser();
        this.password =dbConfigInterface.getJdbcPassword();
        loadDriver();
    }

    /**
     * 加载驱动类
     */
    private void loadDriver(){
        try {
            Class.forName(this.driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
