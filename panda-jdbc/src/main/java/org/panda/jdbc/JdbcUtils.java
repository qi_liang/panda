package org.panda.jdbc;

import com.sun.xml.internal.bind.v2.TODO;
import org.panda.code.uitl.PropsUtil;
import org.panda.code.uitl.StringUtil;
import org.panda.jdbc.helper.DbConfigInterface;
import org.panda.jdbc.mode.Column;
import org.panda.jdbc.mode.DbInfo;
import org.panda.jdbc.mode.Table;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;
import java.util.Properties;

/**
 * @author qi
 */
public class JdbcUtils {

    /**
     * 线程安全
     */
    private static final ThreadLocal<Connection> CONNECTION_HOLDER = new ThreadLocal<>();
    /**
     * 数据源
     */
    private DataSource dataSource;

    public JdbcUtils(DataSource dataSource){
        this.dataSource = dataSource;
    }

    /**
     * 连接数据库
     *
     * @return
     * @throws SQLException
     */
    public Connection getConnection() {
        Connection conn = null;
        if (dataSource!=null){
            try {
                conn = dataSource.getConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return conn;
    }

    /**
     * 返回数据库详情
     *
     * @return
     */
    public DbInfo getDbInfo() {
        Connection conn = null;
        DatabaseMetaData metaData;
        DbInfo dbInfo = new DbInfo();
        try {
            conn = getConnection();
            metaData = conn.getMetaData();
            dbInfo.setName(metaData.getDatabaseProductName());
            dbInfo.setVersion(metaData.getDatabaseProductVersion());
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release( null, null);
        }

        return dbInfo;
    }

    /**
     * 返回所有表名
     */
    public List<Table> getAllTable() {

        List<Table> tableNameList = null;
        Connection conn = null;
        DatabaseMetaData metaData = null;
        ResultSet rs = null;
        ResultSetMetaData resultSetMetaData = null;
        try {
            conn = getConnection();
            //获取数据库名称
            String dbName = conn.getCatalog();
            metaData = conn.getMetaData();
            rs = metaData.getTables(dbName, null, null, new String[]{"TABLE"});
            ResultSetHandler rsh = new BeanListHandler(Table.class);
            tableNameList = (List<Table>) rsh.handler(rs);
            if (tableNameList != null && tableNameList.size() > 0) {
                for (Table table : tableNameList) {
                    ResultSet resultSet = metaData.getColumns(null, null, table.getTableName(), null);
                    ResultSetMetaData crsmd = resultSet.getMetaData();
                    ResultSetHandler columnSetHandler = new BeanListHandler(Column.class);
                    List<Column> columnList = (List<Column>) columnSetHandler.handler(resultSet);
                    table.setColumns(columnList);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release( null, rs);
        }
        return tableNameList;
    }


    /**
     * 更新数据
     *
     * @param sql
     * @param params
     */
    public void update(String sql, Object[] params) {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            st = conn.prepareStatement(sql);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    st.setObject(i + 1, params[i]);
                }
            }

            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release( st, rs);
        }
    }

    /**
     * 批量更新（未开发）
     *
     * @param sql
     */
    public void beatchUpdate(String sql) {
    }

    /**
     * 创建表
     * @param sql
     */
    public void createTable(String sql) {
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            st = conn.createStatement();
            if (StringUtil.isNotNull(sql)) {
                String[] sqlArray = sql.split(";");
                for (String str : sqlArray) {
                    st.executeUpdate(str);
                }
            }
       } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release( st, rs);
        }

    }

    /**
     * 查询
     * @param sql
     * @param params
     * @param rsh
     * @return
     */
    public Object query(String sql, Object[] params, ResultSetHandler rsh) {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            st = conn.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                st.setObject(i + 1, params[i]);
            }
            rs = st.executeQuery();
            return rsh.handler(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release(conn, st, rs);
        }
        return rs;
    }

    /**
     * 关闭数据库连接
     * @param conn
     * @param st
     * @param rs
     */
    public void release(Connection conn, Statement st, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            rs = null;
        }
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (conn != null) {
            try {
                CONNECTION_HOLDER.remove();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     *  关闭数据库连接
     * @param st
     * @param rs （结果集）
     */
    public void release( Statement st, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            rs = null;
        }
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }


}
