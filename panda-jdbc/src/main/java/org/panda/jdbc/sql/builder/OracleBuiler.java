package org.panda.jdbc.sql.builder;


import org.panda.jdbc.mode.Table;
import org.panda.jdbc.sql.PageInterface;
import org.panda.jdbc.sql.builder.AbstractDbBuiler;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author qi
 * Created by qi_liang on 2018/6/2.
 */
public class OracleBuiler extends AbstractDbBuiler {


    @Override
    public String automaticUpdateDb(Set<Class> classSet, List<Table> tableList) {
        return null;
    }

    @Override
    public String concatPageSql(String sql, PageInterface pageInterface) {
        StringBuffer sb=new StringBuffer("select * from (select t1.*,ROWNUM rm from(");
        sb.append(sql);
        Integer index=(pageInterface.getPage()-1)*pageInterface.getPageSize();
        sb.append(")t1 where rownum<="+(index+pageInterface.getPageSize())+") where rm>"+index);
        return sb.toString();
    }

    @Override
    public String createTableStr(Class clazz) {
        return null;
    }

    @Override
    public String insertSql(Object obj) {
        return null;
    }

    @Override
    public String insertSql(Object obj, Map<String, Object> fieldMap) {
        return null;
    }

    @Override
    public String updateSql(Object obj) {
        return null;
    }

    @Override
    public String updateSql(Object obj, Map<String, Object> fieldMap) {
        return null;
    }

    @Override
    public String deleteSql(Object obj) {
        return null;
    }

    @Override
    public String deleteSql(Object obj, Map<String, Object> fieldMap) {
        return null;
    }
}
