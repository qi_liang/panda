package org.panda.jdbc.sql.builder;

import org.panda.jdbc.mode.Table;
import org.panda.jdbc.sql.PageInterface;
import org.panda.jdbc.sql.SqlConstant;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author qi
 *  抽象类-sql构造器
 *  实现的子类有: MysqlBuilder ，OragleBuilder （为应对不同数据库特点，有部分方法抽象，由子类实现）
 * Created by qi_liang on 2018/6/2.
 */
public abstract class AbstractDbBuiler {

    /**
     *  构建更新表结构sql
     * @param classSet   实体类集合
     * @param tableList  数据库已存在表集合
     * @return
     */
    abstract public String automaticUpdateDb(Set<Class> classSet, List<Table> tableList);

    /**
     *   构建分页sql语句
     * @param sql
     * @param pageInterface
     * @return
     */
     abstract public String concatPageSql(String sql, PageInterface pageInterface);


    /**
     * 构建创建表的sql语句
     * @param clazz 对象的class
     * @return 构建好的创建表的sql(类型:String)
     */
    abstract public String createTableStr(Class clazz);

    /**
     *  构建新增sql语句
     * @param obj
     * @return
     */
    abstract public String insertSql(Object obj);

    /**
     *  构建新增sql语句
     * @param obj 
     * @param fieldMap
     * @return
     */
    abstract public String insertSql(Object obj, Map<String,Object> fieldMap);

    /**
     *  构建更新sql语句
     * @param obj
     * @return
     */
    abstract public String updateSql(Object obj);

    /**
     *  构建更新sql语句
     * @param obj
     * @param fieldMap
     * @return
     */
    abstract public String updateSql(Object obj, Map<String,Object> fieldMap);

    /**
     *  构建删除sql
     * @param obj
     * @return
     */
    abstract public String deleteSql(Object obj);

    /**
     *  构建删除sql
     * @param obj
     * @param fieldMap
     * @return
     */
    abstract public String deleteSql(Object obj,Map<String,Object> fieldMap);

    /**
     *  构建统计sql语句
     * @param sql
     * @return
     */
    public String countSql(String sql){
        StringBuffer sb=new StringBuffer("SELECT COUNT(*) FROM ");
        //SQL 转成大写适
        String upperCaseSql = sql.toUpperCase();
        //统计SQL排除排序SQL代码
        if (sql.lastIndexOf(SqlConstant.ORDER_SQL)>sql.lastIndexOf(SqlConstant.SQL_BRACKETS)){
            sb.append(sql.substring(upperCaseSql.indexOf(SqlConstant.FROM_SQL)+SqlConstant.FROM_SQL.length(),upperCaseSql.lastIndexOf(SqlConstant.ORDER_SQL)));
        }else{
            sb.append(sql.substring(upperCaseSql.indexOf(SqlConstant.FROM_SQL)+SqlConstant.FROM_SQL.length()));
        }
        return sb.toString();
    }










}
