package org.panda.jdbc.sql.factory.impl;


import org.panda.jdbc.sql.builder.AbstractDbBuiler;
import org.panda.jdbc.sql.builder.MysqlBuilder;
import org.panda.jdbc.sql.builder.OracleBuiler;
import org.panda.jdbc.sql.SqlConstant;
import org.panda.jdbc.sql.factory.DbBuilerFactory;

/**
 *  @author qi
 *  dbBuilfer 生产工厂模式默认实现
 */
public class DefaultDbBuilerFactory implements DbBuilerFactory {

    @Override
    public AbstractDbBuiler getSqlBuilder(String dbType) {
        AbstractDbBuiler abstractDbBuiler = null;
        switch (dbType){
            case SqlConstant.DB_TYPE_MYSQL:
                abstractDbBuiler = new MysqlBuilder();
            break;
            case SqlConstant.DB_TYPE_ORACLE:
                abstractDbBuiler = new OracleBuiler();
                break;
            default: break;
        }
        return abstractDbBuiler;
    }
}
