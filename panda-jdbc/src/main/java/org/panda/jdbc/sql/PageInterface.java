package org.panda.jdbc.sql;

/**
 *  @author qi
 *  分页抽象类
 */
public interface PageInterface {

    /**
     *  页码
     * @return
     */
    Integer getPage();

    /**
     *  每页数量
     * @return
     */
    Integer getPageSize();
}
