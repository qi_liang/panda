package org.panda.jdbc.sql;


import org.panda.beans.util.BeanUtil;
import org.panda.jdbc.annotation.Table;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author qi
 * 维护类名和数据库字段工具类
 */
public class BeanSqlUtil {

    /**
     *  大小写转化驼峰
     * @param str
     * @return
     */
    public  static String caseToHump(String str){
        StringBuffer strBuffer=new StringBuffer();
        char[] charArray=str.toCharArray();
        for (int i=0,charArrayLenght=charArray.length;i<charArrayLenght;i++){
            if (i==0){
                strBuffer.append(charArray[i]);
                continue;
            }
            if (Character.isUpperCase(charArray[i])){
                strBuffer.append(SqlConstant.SQL_SYMBOL+charArray[i]);
            }else {
                strBuffer.append(charArray[i]);
            }
        }
        return strBuffer.toString().toLowerCase();
    }

    /**
     *  驼峰转大小写
     * @param str
     * @return
     */
    public static String humpToCase(String str){
        StringBuffer strBuffer=new StringBuffer();
        char[] charArray=str.toCharArray();
        for (int i=0;i<charArray.length;i++){
            if (SqlConstant.SQL_SYMBOL.equals(charArray[i])){
                i++;
                strBuffer.append(Character.toUpperCase(charArray[i]));
            }else {
                strBuffer.append(Character.toLowerCase(charArray[i]));
            }
        }
        return strBuffer.toString();
    }

    /**
     *   类名转表名
     * @param entityClazz
     * @return
     */
    public  static String beanNameToTableName(Class entityClazz){
        String tableName = null;
        Table table = (Table) entityClazz.getAnnotation(Table.class);
        if (table==null) {
            //类名
            String entityName = BeanUtil.getEntityName(entityClazz);
            //驼峰转换
            tableName = caseToHump(entityName);
        }else {

            tableName = table.name();
        }
        return tableName;
    }


    /**
     * 返回 key为类属性 value为列名的键值对
     * @param entityClazz
     * @return
     */
    public static Map<String,String> getAttributeAndColumn(Class entityClazz){
        List<Field> fieldsList = BeanUtil.getAllField(entityClazz);
        Iterator iterator = fieldsList.iterator();
        Map<String,String> map=new HashMap<>(fieldsList.size());

        while (iterator.hasNext()){
            Field field = (Field) iterator.next();
            map.put(field.getName(),caseToHump(field.getName()));
        }

        return map;
    }


    /**
     *  对象属性名转换成表列名
     * @param attribute （对象属性）
     * @return  列名
     */
    public  String getColulnmn(String attribute){

        return caseToHump(attribute);
    }
}
