package org.panda.jdbc.sql.builder;

import org.panda.beans.util.BeanUtil;
import org.panda.code.uitl.CollectionUtil;
import org.panda.jdbc.mode.Column;
import org.panda.jdbc.mode.Table;
import org.panda.jdbc.sql.BeanSqlUtil;
import org.panda.jdbc.sql.PageInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author qi
 * Created by qi_liang on 2018/6/2.
 */
public class MysqlBuilder extends AbstractDbBuiler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MysqlBuilder.class);

    private static Map<String,String> dataTypeMap=new HashMap<>();
    /**
     * 数据库数据类型-char
     */
    public static final String DB_TYPE_CHAR = "char";
    /**
     * 数据库数据类型-int
     */
    public static final String DB_TYPE_INT = "int";

    static {
        dataTypeMap.put("class java.lang.String","varchar");
        dataTypeMap.put("class java.util.Date","datetime");
        dataTypeMap.put("class java.lang.Integer","int");
        dataTypeMap.put("class java.lang.Long","bigint");
        dataTypeMap.put("int","int");
    }


    @Override
    public String automaticUpdateDb(Set<Class> classSet,List<Table> tableList) {
        StringBuffer sql = new StringBuffer();
        //扫描实体类包的实体class
        Map<String, Class> map = classSet.stream().collect(Collectors.toMap(BeanSqlUtil::beanNameToTableName,clazz->clazz));
        //初始化的时候,tableList可能为null
        if(tableList!=null&&tableList.size()>0) {
            tableList.forEach(table->{
                if (map.containsKey(table.getTableName())) {
                    Class clazz = map.get(table.getTableName());
                    List<Field> fieldsList = BeanUtil.getAllField(clazz);
                    Map<String, Object> clazzMap = new HashMap<>(fieldsList.size());
                    fieldsList.forEach(field -> {
                        clazzMap.put(BeanSqlUtil.caseToHump(field.getName()), field);
                    });

                    List<Column> columnList = table.getColumns();
                    Iterator<Column> columnIterator = columnList.iterator();
                    while (columnIterator.hasNext()) {
                        Column column = columnIterator.next();
                        if (clazzMap.containsKey(column.getColumnName())) {
                            clazzMap.remove(column.getColumnName());
                            columnIterator.remove();
                        }
                    }
                    columnList.forEach(column -> {
                        sql.append("ALTER TABLE " + table.getTableName() + " DROP COLUMN " + column.getColumnName() + ";");
                    });
                    Set<String> keySet = clazzMap.keySet();
                    keySet.forEach(key->{
                        Field f = (Field) clazzMap.get(key);
                        String column = BeanSqlUtil.caseToHump(f.getName());
                        String columnType = dataTypeMap.get(f.getGenericType().toString());
                        String lengthStr = "";
                        if (DB_TYPE_CHAR.equals(columnType) || DB_TYPE_CHAR.equals(columnType)) {
                            lengthStr = "(100)";
                        }
                        sql.append("ALTER TABLE " + table.getTableName() + " ADD COLUMN " + column + " " + columnType + lengthStr + ";");

                    });
                    map.remove(table.getTableName());
                }
            });
        }
        //创建表
        map.forEach((key,value)->{
            sql.append(createTableStr(value));
        });
        return sql.toString();
    }

    @Override
    public String concatPageSql(String sql, PageInterface pageInterface) {
        StringBuffer sb=new StringBuffer(sql);
        Integer index=(pageInterface.getPage()-1)*pageInterface.getPageSize();
        sb.append(" limit ").append(index).append(","+pageInterface.getPageSize());
        return sb.toString();
    }

    @Override
    public String createTableStr(Class clazz) {
        StringBuffer sql=new StringBuffer("CREATE TABLE ");
        //表名
        String tableName = BeanSqlUtil.beanNameToTableName(clazz);
        sql.append(tableName);
        sql.append("\n(");
        sql.append("id bigint auto_increment not null,\n");
        List<Map<String,String>> mapList= BeanUtil.getFieldInfo(BeanUtil.getField(clazz));
        for (Map<String,String> map:mapList){
            String dataTypeStr = dataTypeMap.get(map.get("fieldType"));
            sql.append(BeanSqlUtil.caseToHump(map.get("fieldName"))+" "+dataTypeStr);
            if (DB_TYPE_CHAR.equals(dataTypeStr)||DB_TYPE_INT.equals(dataTypeStr)){
                if (map.containsKey("fieldLength")) {
                    sql.append("("+map.get("fieldLength")+")");
                }
                else {
                    sql.append("(100)");
                }
            }
            sql.append(",\n");
        }
        sql.append("primary key(id)\n");
        sql.append(")ENGINE=Innodb DEFAULT CHARSET=utf8;");

        return sql.toString();
    }

    @Override
    public String insertSql(Object obj) {
         Map<String,Object> fieldMap = BeanUtil.getFieldValue(obj);
        return insertSql(obj,fieldMap);
    }

    @Override
    public String insertSql(Object obj, Map<String, Object> fieldMap) {
        Class entityClass = obj.getClass();

        if (CollectionUtil.isEmpty(fieldMap)){
            LOGGER.error("can not insert entity:fieldMap is empty");
            throw new RuntimeException("can not insert entity:fieldMap is empty");
           // return null;
        }
        String sql = "INSERT INTO "+ BeanSqlUtil.beanNameToTableName(entityClass);
        StringBuilder columns = new StringBuilder("(");
        StringBuilder values = new StringBuilder("(");
        fieldMap.forEach((fieldName,value)->{
            String columnName = BeanSqlUtil.caseToHump(fieldName);
            columns.append(columnName).append(", ");
            values.append("?, ");
        });
        columns.replace(columns.lastIndexOf(", "),columns.length(),")");
        values.replace(values.lastIndexOf(", "),values.length(),")");
        sql+=columns+" VALUES "+values;
        return sql;
    }

    @Override
    public String updateSql(Object obj) {
        Map<String,Object> fieldMap = BeanUtil.getFieldValue(obj);
        return updateSql(obj,fieldMap);
    }

    @Override
    public String updateSql(Object obj, Map<String, Object> fieldMap) {
        Class entityClass = obj.getClass();
        if (CollectionUtil.isEmpty(fieldMap)){
            LOGGER.error("can not insert entity:fieldMap is empty");
            return null;
        }
        String tableName = BeanSqlUtil.beanNameToTableName(entityClass);
        String sql = "UPDATE "+tableName+" SET ";
        StringBuilder columns = new StringBuilder();
        fieldMap.forEach((fieldName,value)->{
            columns.append(fieldName).append("=?, ");
        });
        sql+=columns.substring(0,columns.lastIndexOf(", "))+" WHERE id = ? ";
        return sql;
    }

    @Override
    public String deleteSql(Object obj) {
        Map<String,Object> fieldMap = BeanUtil.getFieldValue(obj);
        return deleteSql(obj,fieldMap);
    }

    @Override
    public String deleteSql(Object obj, Map<String, Object> fieldMap) {
        Class entityClass = obj.getClass();
        String tableName = BeanSqlUtil.beanNameToTableName(entityClass);
        String sql = "DELETE FROM "+tableName+" WHERE id=?";
        return sql;
    }


}
