package org.panda.jdbc.config;

/**
 * @author qi
 */
public class DbConfigConstant {

    /**
     * 读取配置文件位置
     */
    public static final String CONFIG_FILE="smart.properties";

    public static final String JDBC_DRIVER = "jdbc.driver";

    public static final String JDBC_URL = "jdbc.url";

    public static final String JDBC_USER = "jdbc.user";

    public static final String JDBC_PASSWORD = "jdbc.password";

}
