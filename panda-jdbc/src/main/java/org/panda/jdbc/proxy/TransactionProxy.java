package org.panda.jdbc.proxy;

import org.panda.aop.proxy.Proxy;
import org.panda.aop.proxy.ProxyChain;
import org.panda.jdbc.annotation.Transaction;
import org.panda.jdbc.helper.DataBaseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

/** @author qi
 *  事务代理类
 */
public class TransactionProxy implements Proxy {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionProxy.class);

    private static final ThreadLocal<Boolean> FLAG_HOLDER = new ThreadLocal<Boolean>(){
        @Override
        protected Boolean initialValue() {
            return false;
        }
    };

    @Override
    public Object doProxy(ProxyChain proxyChain) throws Throwable {
        Object result ;
        boolean flag = FLAG_HOLDER.get();
        Method method = proxyChain.getTargetMethod();
        if (!flag&& method.isAnnotationPresent(Transaction.class)){
            try {
                FLAG_HOLDER.set(true);
                //开启事务
                DataBaseHelper.beginTransaction();
                LOGGER.debug("begin transaction");
                result = proxyChain.doProxhChain();
                //提交事务
                DataBaseHelper.commitTransaction();
                LOGGER.debug("commit transaction");
            }catch (Exception e){
                //事务回滚
                DataBaseHelper.rollbackTransation();
                LOGGER.debug("rollback transaction");
                throw e;
            }finally {
                FLAG_HOLDER.remove();
            }
        }else {
            result = proxyChain.doProxhChain();
        }
        return result;
    }
}
