package org.panda.jdbc.annotation;

import java.lang.annotation.*;

/**
 *  列名详情注解
 * @author qi
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Column {

    /**
     *  默认值
     * @return
     */
    String defaultValue() default "";

    /**
     *  字段长度
     * @return
     */
    int length() default 50;
    /**
     * 是否为空 false:NOT NULL ：true:NULL
     * 默认 true
     */
    boolean isNull() default true;
}
