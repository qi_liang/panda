package org.panda.jdbc.annotation;

import java.lang.annotation.*;

/**
 * 数据库表注解
 * @author qi
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Table {
    /**
     *  ENGINE 设置存储引擎
     *   类型: ISAM  MyISAM  HEAP InnoDB
     *  相关链接：https://www.cnblogs.com/zhangjinghe/p/7599988.html
     * @return
     */
    String engine() default "InnoDB";

    /**
     * CHARSET 设置编码
     * @return
     */
    String charset() default "utf-8";

    /**
     * 表名
     * @return
     */
    String name();
}
