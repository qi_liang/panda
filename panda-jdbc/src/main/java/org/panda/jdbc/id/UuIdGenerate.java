package org.panda.jdbc.id;

import java.util.UUID;

/**
 * @Description uuid主键生成
 * @Author qi
 **/
public class UuIdGenerate implements IdGenerate<String> {
    @Override
    public String nextId() {
        return UUID.randomUUID().toString();
    }
}
