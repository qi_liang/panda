package org.panda.jdbc.id;

/**
 * 数据库主键ID生成器
 * @author qi
 */
public interface IdGenerate<T> {

    /**
     *
     * @return 主键ID
     */
    T nextId();
}
