package org.panda.jdbc;

import org.panda.beans.util.BeanUtil;
import org.panda.jdbc.mode.Column;
import org.panda.jdbc.mode.Table;
import org.panda.jdbc.sql.BeanSqlUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 8:44 AM 2019/11/2
 */
public class TableUtil {

    /**
     *  class 对象转换成 table对象
     * @param clazz
     * @return
     */
    public Table beanToTable(Class clazz){
        Table table = new Table();
        String tableName = BeanSqlUtil.beanNameToTableName(clazz);
        table.setTableName(tableName);
        List<Field> fields = BeanUtil.getAllField(clazz);
        List<Column> columnList = new ArrayList<>();
        fields.forEach(field -> {
            Column column = fieldToColumn(field);
            columnList.add(column);
        });

        table.setColumns(columnList);

        return table;
    }

    /**
     * 对象属性转换成 表列
     * @param field
     * @return
     */
    public Column fieldToColumn(Field field){
        Column column = new Column();
        if (field.isAnnotationPresent(org.panda.jdbc.annotation.Column.class)){
            org.panda.jdbc.annotation.Column annotationColumn = field.getAnnotation(org.panda.jdbc.annotation.Column.class);
            int length = annotationColumn.length();
            String defaultValue = annotationColumn.defaultValue();
            boolean isNull = annotationColumn.isNull();
            column.setColumnSize(length);
            column.setColumnDef(defaultValue);
            column.setColumnName(BeanSqlUtil.caseToHump(field.getName()));
            column.setNull(isNull);
        }

        return column;
    }
}
