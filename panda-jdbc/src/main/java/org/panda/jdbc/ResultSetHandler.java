package org.panda.jdbc;

import java.sql.ResultSet;

/**
 * @author qi
 * 结果集处理接口
 */
public interface ResultSetHandler {


	/**
	 * 结果集映射成对象
	 * @param rs（jdbc查询结果集）
	 * @return 对象
	 */
	 Object handler(ResultSet rs);

}
