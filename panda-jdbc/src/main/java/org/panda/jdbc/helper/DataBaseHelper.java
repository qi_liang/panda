package org.panda.jdbc.helper;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.panda.beans.util.BeanUtil;
import org.panda.jdbc.sql.builder.AbstractDbBuiler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author qi
 * 数据库助手类
 */
public class DataBaseHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataBaseHelper.class);
    /**线程安全**/
    private static final ThreadLocal<Connection> CONNECTION_HOLDER = new ThreadLocal<>();
    /**数据库连接池**/
    private static final BasicDataSource DATA_SOURCE;


    private static final QueryRunner QUERY_RUNNER = new QueryRunner();

    private static final String JDBC_DRIVER;

    private static final String JDBC_URL;

    private static final String JDBC_USER;

    private static final String JDBC_PASSWORD;

    static {
//        JDBC_DRIVER = DbConfigHelper.getJdbcDriver();
//        JDBC_URL = DbConfigHelper.getJdbcUrl();
//        JDBC_USER = DbConfigHelper.getJdbcUser();
//        JDBC_PASSWORD = DbConfigHelper.getJdbcPassword();
        JDBC_DRIVER = null;
        JDBC_URL = null;
        JDBC_USER = null;
        JDBC_PASSWORD = null;
        DATA_SOURCE = new BasicDataSource();
        DATA_SOURCE.setDriverClassName(JDBC_DRIVER);
        DATA_SOURCE.setUrl(JDBC_URL);
        DATA_SOURCE.setUsername(JDBC_USER);
        DATA_SOURCE.setPassword(JDBC_PASSWORD);
    }

    /**
     * 开启事务
     */
    public static void beginTransaction() {
        Connection conn = getConnection();
        try {
            conn.setAutoCommit(false);
        } catch (SQLException e) {
            LOGGER.error("begin transation failure", e);
            e.printStackTrace();
        } finally {
            CONNECTION_HOLDER.set(conn);
        }
    }

    /**
     * 提交事务
     */
    public static void commitTransaction() {
        Connection conn = getConnection();
        if (conn != null) {
            try {
                conn.commit();
                conn.close();
            } catch (SQLException e) {
                LOGGER.error("begin transation failure", e);
                e.printStackTrace();
            }
        }
    }

    /**
     * 回滚事务
     */
    public static void rollbackTransation() {
        Connection conn = getConnection();
        if (conn != null) {
            try {
                conn.rollback();
                conn.close();
            } catch (SQLException e) {
                LOGGER.error("rollback transaction failure", e);
                throw new RuntimeException();
                //  e.printStackTrace();
            } finally {
                CONNECTION_HOLDER.remove();
            }
        }
    }

    /**
     * 获取数据库连接
     *
     * @return
     */
    public static Connection getConnection() {
        Connection conn = CONNECTION_HOLDER.get();
        if (conn != null) {
            try {
//                conn = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
                conn = DATA_SOURCE.getConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                CONNECTION_HOLDER.set(conn);
            }
        }
        return conn;
    }

//    public static void closeConnection(){
//        Connection conn = CONNECTION_HOLDER.get();
//        if (conn!=null){
//            try {
//                conn.close();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    /**
     * 查询对象集合
     *
     * @param entityClass
     * @param sql
     * @param params
     * @param <T>
     * @return
     */
    public static <T> List<T> queryEntityList(Class<T> entityClass, String sql, Object... params) {
        Connection conn = getConnection();
        List<T> entityList = null;
        try {
            entityList = QUERY_RUNNER.query(conn, sql, new BeanListHandler<>(entityClass), params);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        return entityList;
    }

    /**
     * 查询一个对象
     *
     * @param entityClass
     * @param sql
     * @param params
     * @param <T>
     * @return
     */
    public static <T> T queryEntity(Class<T> entityClass, String sql, Object... params) {
        T entity = null;
        Connection conn = getConnection();
        try {
            entity = QUERY_RUNNER.query(conn, sql, new BeanHandler<T>(entityClass), params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entity;
    }

    /**
     * 连表查询
     */
    public static List<Map<String, Object>> executeQuery(String sql, Object... params) {
        List<Map<String, Object>> result = null;

        Connection conn = getConnection();
        try {
            result = QUERY_RUNNER.query(conn, sql, new MapListHandler(), params);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 执行更新语句
     *
     * @param sql
     * @param params
     * @return
     */
    public static int executeUpdate(String sql, Object... params) {
        int rowNum = 0;
        Connection conn = getConnection();
        try {
            rowNum = QUERY_RUNNER.update(conn, sql, params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowNum;
    }

    /**
     *  新增数据
     * @param obj
     * @param abstractDbBuiler
     * @return
     */
    public static boolean insertEntity(Object obj, AbstractDbBuiler abstractDbBuiler) {
        Map<String, Object> fieldMap = BeanUtil.getFieldValue(obj);
        String sql = abstractDbBuiler.insertSql(obj, fieldMap);
        Object[] params = fieldMap.values().toArray();
        return executeUpdate(sql, params) == 1;
    }

    /**
     *  更新数据
     * @param obj
     * @param abstractDbBuiler
     * @param id
     * @return
     */
    public static boolean updateEntity(Object obj, AbstractDbBuiler abstractDbBuiler, Long id) {
        Map<String, Object> fieldMap = BeanUtil.getFieldValue(obj);
        String sql = abstractDbBuiler.insertSql(obj, fieldMap);
        List<Object> paramList = new ArrayList<>();
        paramList.addAll(fieldMap.values());
        paramList.add(id);
        Object[] params = paramList.toArray();
        return executeUpdate(sql, params) == 1;
    }

    /**
     * 删除数据
     * @param obj
     * @param abstractDbBuiler
     * @param id
     * @return
     */
    public static boolean deleteEntity(Object obj, AbstractDbBuiler abstractDbBuiler, Long id) {
        Map<String, Object> fieldMap = BeanUtil.getFieldValue(obj);
        String sql = abstractDbBuiler.deleteSql(obj, fieldMap);
        return executeUpdate(sql, id) == 1;
    }

}
