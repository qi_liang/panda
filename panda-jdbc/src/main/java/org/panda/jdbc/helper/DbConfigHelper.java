package org.panda.jdbc.helper;

import org.panda.code.uitl.PropsUtil;
import org.panda.jdbc.config.DbConfigConstant;

import java.util.Properties;

/**
 * @author qi
 * 属性文件助手类
 */
public class DbConfigHelper implements DbConfigInterface {

    private static final Properties CONFIG_PROPS = PropsUtil.loadProps(DbConfigConstant.CONFIG_FILE);

    /**
     * 获取数据库驱动
     */
    @Override
    public  String getJdbcDriver() {

        return PropsUtil.getString(CONFIG_PROPS, DbConfigConstant.JDBC_DRIVER);
    }

    /**
     * 获取数据库URL
     *
     * @return
     */
    @Override
    public  String getJdbcUrl() {

        return PropsUtil.getString(CONFIG_PROPS, DbConfigConstant.JDBC_URL);
    }

    /**
     * 获取数据库用户
     *
     * @return
     */
    @Override
    public  String getJdbcUser() {

        return PropsUtil.getString(CONFIG_PROPS, DbConfigConstant.JDBC_USER);
    }

    /**
     * 获取数据库密码
     */
    @Override
    public  String getJdbcPassword() {

        return CONFIG_PROPS.getProperty(DbConfigConstant.JDBC_PASSWORD);
    }


}
