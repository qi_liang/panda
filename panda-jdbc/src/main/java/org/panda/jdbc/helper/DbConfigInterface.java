package org.panda.jdbc.helper;

/**
 * @Author: qi
 * @Description: 数据库配置文件接口
 * @Date: Create in 9:00 AM 2019/8/14
 */
public interface DbConfigInterface {

    /**
     *  返回数据库驱动
     * @return
     */
    String getJdbcDriver();

    /**
     * 返回数据库连接地址
     * @return
     */
    String getJdbcUrl();

    /**
     *  返回数据库用户
     * @return
     */
    String getJdbcUser();

    /**
     *  返回数据库密码
     * @return
     */
    String getJdbcPassword();
}
