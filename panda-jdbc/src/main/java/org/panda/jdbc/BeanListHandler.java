package org.panda.jdbc;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

/**
 * @author qi
 * 结果集（集合）处理类
 */
public class BeanListHandler implements ResultSetHandler {

	private Class<?> clazz;

	private ResultSetHandler resultSetHandler;

	public BeanListHandler(Class<?> clazz) {
		this.clazz = clazz;
		this.resultSetHandler = new BeanHander(clazz);
	}

	@Override
	public Object handler(ResultSet rs) {

		try {
			List<Object> list = new ArrayList<>();
			while (rs.next()) {
				Object bean = resultSetHandler.handler(rs);
				list.add(bean);
			}
			return list.size() > 0 ? list : null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

}
