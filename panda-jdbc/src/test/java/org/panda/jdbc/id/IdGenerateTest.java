package org.panda.jdbc.id;

import junit.framework.TestCase;

public class IdGenerateTest extends TestCase {

    public void testNextId() {

        IdGenerate<String> stringIdGenerate = new UuIdGenerate();
        for (int i=0;i<10000;i++){
            System.out.println(stringIdGenerate.nextId());
        }
        IdGenerate<Long> longIdGenerate = new SnowflakeIdGenerate(0,0);
        for (int i=0;i<10000;i++){
            System.out.println(longIdGenerate.nextId());
        }
    }
}