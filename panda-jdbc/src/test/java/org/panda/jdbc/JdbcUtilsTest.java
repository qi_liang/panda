package org.panda.jdbc;

import org.junit.Test;
import org.panda.entity.SysUser;
import org.panda.jdbc.annotation.Column;
import org.panda.jdbc.sql.builder.AbstractDbBuiler;
import org.panda.jdbc.sql.SqlConstant;
import org.panda.jdbc.sql.factory.DbBuilerFactory;
import org.panda.jdbc.sql.factory.impl.DefaultDbBuilerFactory;

import java.lang.reflect.Field;

public class JdbcUtilsTest {

    @Test
    public void getConnection() {
    }

    @Test
    public void getDBInfo() {
    }

    @Test
    public void getAllTable() {
    }

    @Test
    public void update() {
    }

    @Test
    public void beatchUpdate() {

    }

    @Test
    public void createTable() {

        Class clzz = SysUser.class;

        Field[] fields = clzz.getDeclaredFields();
        for (Field field1:fields){
            if (field1.isAnnotationPresent(Column.class)){
                Column column = field1.getAnnotation(Column.class);
                System.out.println(column);
            }
          //  System.out.println(column.length());
        }

        DbBuilerFactory dbBuilerFactory = new DefaultDbBuilerFactory();
        AbstractDbBuiler abstractDbBuiler = dbBuilerFactory.getSqlBuilder(SqlConstant.DB_TYPE_MYSQL);
        String createSql = abstractDbBuiler.createTableStr(SysUser.class);
        System.out.println(createSql);
    }

    @Test
    public void query() {
    }

    @Test
    public void release() {
    }

    @Test
    public void release1() {
    }
}