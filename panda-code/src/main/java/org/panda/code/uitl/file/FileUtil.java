package org.panda.code.uitl.file;


import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.panda.code.uitl.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

/**
 * @author qi
 * 文件工具类
 */
public class FileUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);

    /**
     * 获取真实文件名(自动去掉文件路径)
     * @param fileName
     * @return
     */
    public static String getRealFileName(String fileName){
        return FilenameUtils.getName(fileName);
    }

    /**
     *  创建文件
     * @param filePath
     * @return
     */
    public static File createFile(String filePath){
        File file;

        file = new File(filePath);
        File parentDir = file.getParentFile();
        if (!parentDir.exists()){
            try {
                FileUtils.forceMkdir(parentDir);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    /**
     *  读取文件内容
     * @param filePath
     * @return
     */
    public static String read(String filePath){
        StringBuffer stringBuffer=new StringBuffer();
        try {
            FileReader reader=new FileReader(filePath);
            while (reader.ready()){
                stringBuffer.append((char) reader.read());
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }

        return stringBuffer.toString();
    }

    /**
     *  文件写入
     * @param str
     * @param filePath
     * @return
     */
    public  static boolean writer(String str,String filePath){
        boolean result=false;
        try {
            FileWriter fileWriter=new FileWriter(filePath,true);
            fileWriter.write(str);
            fileWriter.flush();
            fileWriter.close();
            result=true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  result;
    }

    /**
     *  获取文件格式
     * @param fileName
     * @return
     */
    public static String fileFormat(String fileName){
        if (StringUtil.isNotNull(fileName)) {
            String[] strArray = fileName.split("\\.");
            return strArray[strArray.length - 1];
        }
        return null;
    }

    /**
     *  根据文件名返回文件类型
     * @param fileName
     * @return
     */
    public static String getFileType(String fileName){
        String fileType=null;
        if (StringUtil.isNotNull(fileName)){
            String fileFormat=fileFormat(fileName);
            switch (fileFormat.toUpperCase()){
                case "BMP":
                case "JPEG":
                case "JPG":
                case "ICO":
                case "PNG":
                case "JNG":
                   fileType = FileTypeEnum.FILE_TYPE_IMAGE.getValue();break;
                case "AVI":
                case "MOV":
                case "FLV":
                case "MP4":
                case "MPG":
                    fileType = FileTypeEnum.FILE_TYPE_VIEDO.getValue();break;
                case "PDF":
                    fileType = FileTypeEnum.FILE_TYPE_PDF.getValue();break;
                default:
                    fileType = FileTypeEnum.FILE_TYPE_OTHER.getValue();break;
            }
        }

        return fileType;
    }

    /**
     * 获取一个文件夹下的文件目录
     * @param dirPath
     * @return
     */
    public static void getFileList(String dirPath){

        File file = new File(dirPath);
        if (!file.exists()){
            String msg = "file not exists,please check file !" ;
            LOGGER.error(msg,new RuntimeException(msg));
        }
        if (!file.isDirectory()){
            String msg = "file is not directory, please check file !";
            LOGGER.error(msg,new RuntimeException(msg));
        }
        File[] files = file.listFiles();
        for (File file1:files){
            if (file1.isDirectory()){
                System.out.println(file.getName());
                getFileList(file1.getPath());
                continue;
            }
            System.out.println(file1.getName());
        }
    }
    /**
     * 获取一个文件夹下的隐藏文件列表
     * @param dirPath
     * @return
     */
    public static List<File> findAllHiddenFile(String dirPath){

        File dirFile = new File(dirPath);

        return null;
    }

    public static void main(String[] args) {

                getFileList("/Users/qi/java");
    }
}
