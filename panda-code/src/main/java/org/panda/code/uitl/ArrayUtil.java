package org.panda.code.uitl;

/**
 * @author qi
 * 数组工具类
 */
public class ArrayUtil {

    /**
     *  判断数组是否非空
     * @param array
     * @return
     */
    public static<T> boolean isNotEmpty(T[] array){

        return array!=null&&array.length>0;
    }

    /**
     *  判断数组是否为空
     * @param array
     * @return
     */
    public static boolean isEmpty(Object[] array){

        return array==null||array.length==0;
    }

    /**
     *  生成数组
     * @param values
     * @param <T>
     * @return
     */
    public static <T> T [] of(T... values){

        return values;
    }

}
