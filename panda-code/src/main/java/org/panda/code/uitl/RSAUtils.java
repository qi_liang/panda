package org.panda.code.uitl;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * java RSA加密工具类
 * @author qi
 **/
public class RSAUtils {
    /**
     * 密钥长度,越长越慢
     */
    private final static int KEY_SIZE = 1024;
    /**
     * 算法-RSA
     */
    public static final String ALGORITHM_RSA = "RSA";
    /**
     * 默认字符集编码 utf-8
     */
    public static final String DEFAULT_CHART="utf-8";


    /**
     * 用于封装随机参数的公钥和私钥
     */
    private static Map<Integer,String> keyMap = new HashMap<>();

    /**
     * 生成随机密钥对
     * @throws NoSuchAlgorithmException
     */
    public static void getKeyPair() throws NoSuchAlgorithmException {
        //keyPairGenerator类用于生成公钥和密钥对,基于RSA算法生成对象
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITHM_RSA);
        //初始化密钥生成器
        keyPairGenerator.initialize(KEY_SIZE,new SecureRandom());
        //生成密钥对,保存在keyPair中
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        //私钥
        RSAPrivateKey privateKey =(RSAPrivateKey)keyPair.getPrivate();
        //公钥
        RSAPublicKey publicKey =(RSAPublicKey) keyPair.getPublic();
        //得到密钥字符串
        String publicKeyStr = Base64.getEncoder().encodeToString(publicKey.getEncoded());
        String privateKeyStr = Base64.getEncoder().encodeToString(privateKey.getEncoded());
        //将密钥保存到map中
        //0表示公钥
        keyMap.put(0,publicKeyStr);
        //1表示密钥
        keyMap.put(1,privateKeyStr);
    }

    /**
     *  RSA公钥加密
     * @param str
     * @param publicKey
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws UnsupportedEncodingException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public static String encrypt(String str,String publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
        byte[] decoded = Base64.getDecoder().decode(publicKey);
        RSAPublicKey pubKey = (RSAPublicKey) KeyFactory.getInstance(ALGORITHM_RSA).generatePublic(new X509EncodedKeySpec(decoded));
        Cipher cipher = Cipher.getInstance(ALGORITHM_RSA);
        cipher.init(Cipher.ENCRYPT_MODE,pubKey);
        String outStr = Base64.getEncoder().encodeToString(cipher.doFinal(str.getBytes(DEFAULT_CHART)));
        return outStr;
    }

    public static String decrypt(String str,String privateKey) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        //64位解码加密后的字符串
        byte[] inputByte = Base64.getDecoder().decode(str);
        //base64编码的密钥
        byte[] decoded = Base64.getDecoder().decode(privateKey);
        RSAPrivateKey prikey = (RSAPrivateKey) KeyFactory.getInstance(ALGORITHM_RSA).generatePrivate(new PKCS8EncodedKeySpec(decoded));
        Cipher cipher = Cipher.getInstance(ALGORITHM_RSA);
        cipher.init(Cipher.DECRYPT_MODE,prikey);
        String outStr = new String(cipher.doFinal(inputByte));
        return outStr;
    }

}
