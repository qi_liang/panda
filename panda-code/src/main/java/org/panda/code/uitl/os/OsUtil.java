package org.panda.code.uitl.os;

import org.panda.code.uitl.os.mode.OsInfo;

import java.util.Properties;

/**
 * @author qi
 * 获取计算机操作系统信息
 * Created by qi_liang on 2018/6/9.
 */
public class OsUtil {
    static Properties properties = System.getProperties();


//
//      public OsUtil(OperatingSystem os){
//          this.os = os;
//
//      }
//
//    /**
//     *  操作系统
//     */
//    public static String getArch(){
//        return os.getArch();
//    }
//
//    /**
//     *
//     */
//    public static String getCpuEndian(){
//        return os.getCpuEndian();
//    }
//
//    public static String getDateModel(){
//
//        return os.getDataModel();
//    }

    public static OsInfo getInfo(){
        OsInfo osInfo = new OsInfo();
        osInfo.setOsVersion(properties.getProperty("os.version"));
        osInfo.setOsName(properties.getProperty("os.name"));
        osInfo.setArch(properties.getProperty("os.arch"));
        return osInfo;
    }




}
