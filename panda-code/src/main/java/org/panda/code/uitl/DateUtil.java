package org.panda.code.uitl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期工具类
 * @author qi
 */
public class DateUtil {
    /**
     * 日志处理类
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(DateUtil.class);
    /**
     * 默认日期格式
     */
    public static final String DEFAULT_PATTERN="yyyy-MM-dd HH:mm:ss";

    /**
     *  返回日期 的字符串 默认格式:yyy-MM-dd HH:mm:ss
     * @param date
     * @return
     */
    public static String getDateToStr(Date date){
        if (date==null){
            throw new NullPointerException("========date 对象为空null======");
        }

        return getDateToStr(date,DEFAULT_PATTERN);
        }

    /**
     *  返回日期的字符串
      * @param date     日期
     * @param pattern   格式 default:yyy-MM-dd HH:mm:ss
     * @return
     */
    public static String getDateToStr(Date date,String pattern){
        if (StringUtil.isEmpty(pattern)){
            pattern = DEFAULT_PATTERN;
        }
        SimpleDateFormat sdf=new SimpleDateFormat(pattern);
        if (date == null){
            throw new NullPointerException("====== date 对象为空 null======");
        }
        String str=sdf.format(date);
        return str;
    }

    /**
     *  通过日期字符串返回时间
     * @param strDate
     * @param pattern
     * @return
     * @throws ParseException
     */
    public static Date strToDate(String strDate,String pattern) throws ParseException {
        if (StringUtil.isEmpty(pattern)){
                pattern = DEFAULT_PATTERN;
        }

        SimpleDateFormat sdf=new SimpleDateFormat(pattern);

        return sdf.parse(strDate);
    }

    /**
     * 两个日期相差多少日
     * @param startDate
     * @param endDate
     * @return
     */
    public static int disparityDay(Date startDate,Date endDate){
        long endTime = endDate.getTime();
        long starTime = startDate.getTime();
        if (endTime>starTime) {
            int day = new Long((endDate.getTime() - startDate.getTime()) / (24 * 60 * 60 * 1000)).intValue();
            return day;
        }
        return 0;
    }

    /**
     *  时间戳转时间字符串
     * @param timeLong
     * @return
     */
    public static String dataFormat(Long timeLong){
        Long secondLong = timeLong/1000;
        Long  millisecond = timeLong%1000;
        Long second = secondLong%60;
        Long minuteLong =  secondLong/60;
        Long minute = second%60;
        Long hour =  minuteLong/60;
        DecimalFormat df = new DecimalFormat("00");
        return df.format(hour)+":"+df.format(minute)+":"+df.format(second);


    }


}


