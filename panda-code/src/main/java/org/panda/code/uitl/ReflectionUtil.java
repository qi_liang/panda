package org.panda.code.uitl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author qi
 * 反射工具
 * newInstance: 弱类型。低效率。只能调用无参构造。
   new: 强类型。相对高效。能调用任何public构造。
 */
public class ReflectionUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReflectionUtil.class);

    /**
     *  创建实例
     * @param clazz
     * @return
     */
    public static Object newInstance(Class clazz){
        Object instance = null ;
        try {
            instance = clazz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return instance;
    }

    /**
     * 调用方法
     * @param obj     调用对象
     * @param method  调用方法
     * @param args    参数
     * @return
     */
    public static Object invokeMethod(Object obj, Method method,Object... args){
        Object result = null;
        try {
            method.setAccessible(true);
            result = method.invoke(obj,args);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     *  设置成员变量
     * @param obj
     * @param field
     * @param value
     */
    public static void setField(Object obj,Field field,Object value){

        try {
            field.setAccessible(true);
            field.set(obj,value);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
