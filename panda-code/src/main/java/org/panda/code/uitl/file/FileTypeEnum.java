package org.panda.code.uitl.file;

/**
 * @author qi
 * Created by qi_liang on 2018/4/1.
 */
public enum  FileTypeEnum {
    //文件类型-图片
    FILE_TYPE_IMAGE("IMAGE","图片"),
    //文件类型-视频
    FILE_TYPE_VIEDO("VIEDO","视频"),
    //文件类型-电子书
    FILE_TYPE_PDF("PDF","电子书"),
    //其他文件类型
    FILE_TYPE_OTHER("OTHER","其他")
    ;

    private String value;
    private String desc;

    FileTypeEnum(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
