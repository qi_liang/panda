package org.panda.code.uitl;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 图片处理工具类
 * @author qi
 */
public class ImageUtil {


    /**
     *
     * @param filePath  png图片路径
     * @param outPrintFilePath 转换后文件输出路径
     */
    public static void png2jpeg(String filePath,String outPrintFilePath) {
            //读取图片
            FileOutputStream fos =null;
            try {
                BufferedImage bufferedImage = ImageIO.read(new File(filePath));
                //转成jpeg、
                BufferedImage bufferedImage1 = new BufferedImage(bufferedImage.getWidth(),
                        bufferedImage.getHeight(),
                        BufferedImage.TYPE_INT_RGB);
                bufferedImage1.createGraphics().drawImage(bufferedImage,0,0, Color.white,null);
                fos = new FileOutputStream(outPrintFilePath);
                ImageIO.write(bufferedImage,"jpg",fos);
                fos.flush();
            } catch (IOException e) {
                e.printStackTrace();
                try {
                    fos.close();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        }

    /**
     * jpg转png
     * @param filePath  jpg文件路径
     * @param outPrintFilePath 转换后文件输出路径
     */
    public static void jpeg2png(String filePath,String outPrintFilePath) {
            //读取图片
            try {
                BufferedImage bufferedImage = ImageIO.read(new File(filePath));
                //转成png、
                BufferedImage bufferedImage1 = new BufferedImage(bufferedImage.getWidth(),
                        bufferedImage.getHeight(),
                        BufferedImage.TYPE_INT_ARGB);
                bufferedImage1.createGraphics().drawImage(bufferedImage,0,0, Color.white,null);
                FileOutputStream fos = new FileOutputStream(outPrintFilePath);
                ImageIO.write(bufferedImage1,"png",fos);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

}
