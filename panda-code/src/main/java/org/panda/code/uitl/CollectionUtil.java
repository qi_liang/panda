package org.panda.code.uitl;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author qi
 * 集合工具类
 */
public class CollectionUtil {

    /**
     *  判断 collection 是否为null
     * @param collection
     * @return
     */
    public static boolean isEmpty(Collection<?> collection){

        return CollectionUtils.isEmpty(collection);
    }

    /**
     *  判断 集合是否不为 null
     * @param collection
     * @return
     */
    public static boolean isNotEmpty(Collection<?> collection){

        return CollectionUtils.isNotEmpty(collection);
    }

    /**
     * 判断Map是否为空
     * @param map
     * @return
     */
    public static boolean isEmpty(Map<?,?> map){

        return MapUtils.isEmpty(map);
    }

    /**
     *  判断Map是否不为空
     * @param map
     * @return
     */
    public static boolean isNotEmpty(Map<?,?> map){
        return !isEmpty(map);
    }

    /**
     * list分页
     * @param list
     * @param page
     * @param pageSize
     * @return
     */
    public static <T> List<T> subPageList(List<T> list,int page,int pageSize){
        List<T> pageList = new ArrayList(pageSize);
        //list不为NULL而且list的size>0
        if (list!=null) {
            if (list.size()>0&&page>0&&pageSize>0) {
                //总页数
                int count = list.size();
                int pageCount = count / pageSize;
                if ((pageCount % pageSize) != 0) {
                    pageCount++;
                }
                if (page <= pageCount) {
                    //开始索引
                    int startIndex;
                    //结束索引
                    int endIndex;
                    endIndex = (page * pageSize);
                    startIndex = endIndex - pageSize;
                    if (startIndex < 0) {
                        startIndex = 0;
                    }
                    pageList = list.subList(startIndex, endIndex);
                }
            }else {
                pageList.addAll(list);
            }
        }
        return  pageList;
    }

}
