package org.panda.code.uitl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;


/**
 * 流操作工具类
 * @author qi
 */
public class  StreamUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(StreamUtil.class);

    /**
     *  默认缓存块大小
     */
    public static final int BUFFER_SIZE = 4096;

    /**
     *  输入流转换字符串
     * @param is
     * @return
     */
    public static String getString(InputStream is){
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = reader.readLine())!=null){
                sb.append(line);
            }
        }catch (IOException e){
            LOGGER.error("get string failure",e);
            throw new RuntimeException(e);
        }

        return sb.toString();
    }

    /**
     * 输入流复制到输出流
     */
    public static void copyStream(InputStream inputStream, OutputStream outputStream){
        try {
            int length;
            byte [] buffer = new byte[5*1024];
            while ((length = inputStream.read(buffer,0,buffer.length))!=-1){
                outputStream.write(buffer,0,length);
            }
            outputStream.flush();
        }catch (Exception e){
            LOGGER.error("copy stram failure",e);
            throw new RuntimeException(e);
        }finally {
            try {
                inputStream.close();
                outputStream.close();
            } catch (IOException e) {
                LOGGER.error("close stream failure",e);
                throw new RuntimeException(e);
            }
        }
    }

    /**
     *  缓存块写入输出流中
     * @param in
     * @param out
     * @throws IOException
     */
    public static void copy(byte[] in,OutputStream out) throws IOException {
        if (in==null||in.length==0){
            LOGGER.error("in is null");
        }
        if (out==null){
            LOGGER.error("out is null");
        }
        out.write(in);
    }


}
