package org.panda.code.uitl.os;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author qi
 * 获取计算机网络信息
 * Created by qi_liang on 2018/6/9.
 */
public class NetUtil {

    /**
     * 获取IP
     * @return
     */
    public static String getIp(){
        String ip = null;
        try {
            InetAddress address = InetAddress.getLocalHost();
            ip = address.getHostAddress();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return  ip;
    }

}
