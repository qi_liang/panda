package org.panda.code.uitl;


import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author qi
 * Created by qi on 2017/8/13.
 */
public class StringUtil {

    public static final String SEPARATOR = String.valueOf((char)29);

    /**
     *  判断两个字符串是否相等
     * @param str1
     * @param str2
     * @return
     */
    public static boolean isEqual(String str1,String str2){

      if (isEmpty(str1)||isEmpty(str2)){
          return  false;
      }
        return  str1.equals(str2);
    }

    /**
     *   判断字符串为空
     * @param str1
     * @return 为空返回true
     */
    public static boolean isEmpty(String str1){

        return str1==null||str1.length()==0;
    }

    /**
     *  判断字符串不为空
     * @param str1
     * @return 不为空 返回true
     */
    public  static boolean isNotNull(String str1){

        return !isEmpty(str1);
    }

    /**
     *   随机产生char
     *
     * @param startInt 在ASCII（十进制）开始的位置
     * @param range 随机数的范围
     * @return
     */
    public static char randomChar(int startInt,int range){
      //  Random random=new Random();
        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
        int randomNum=threadLocalRandom.nextInt(range-1);
        int randomInt=randomNum+startInt;
        return (char)randomInt ;
    }

    /**
     *  返回 英文大小写混合随机
     * @param length
     * @return
     */
    public static String randomString(int length){
        StringBuffer randomString=new StringBuffer();
        while (length>0){
            ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
            int ran=threadLocalRandom.nextInt(3);
            switch (ran){
                case 0:
                    randomString.append(threadLocalRandom.nextInt(10));
                    break;
                case 1:
                    randomString.append(randomLowerCase());
                    break;
                case 2:
                    randomString.append(randomUpperCase());
                    break;
                default:
                    break;
            }
            length--;
        }
        return randomString.toString();
    }

    /**
     *   随机小写字母
     * @return
     */
    public static char randomLowerCase(){
        return randomChar((int)'a',26);
    }

    /**
     *  随机大写字母
     * @return
     */
    public  static char randomUpperCase(){
        return randomChar((int)'A',26);
    }

    /**
     * 字符串切割
     * @param str
     * @param sign
     * @return
     */
    public static String[] splistString(String str,String sign){

        return str.split(sign);
    }
    /**
     *   随着返回数字
     * @param
     * @param
     * @return
     */
    public static char randomNumber(){

        return randomChar('1',10);
    }

    public String randomString(int length,String needRule){
        //jdk 1.7 以后用来取代Random,性能会有提升
        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
        threadLocalRandom.nextInt(100);
        char a1='a';
        int i=a1;

        System.out.println(i);

        return  null;
    }


    /**
     * 统计字符在字符串出线次数
     * @param str
     * @param ch
     * @return
     */
    public static int countChar(String str,char ch){
        char[] chars = str.toCharArray();
        Map<Character,Integer> countMap = new HashMap<>(chars.length);
        for (char cha:chars){
            if (countMap.containsKey(cha)){
                int contNum = countMap.get(cha);
                contNum++;
                countMap.put(cha,contNum);
            }else {
                countMap.put(cha,new Integer(1));
            }
        }

        return countMap.get(ch);
    }

}
