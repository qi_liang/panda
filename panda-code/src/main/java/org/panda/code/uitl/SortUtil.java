package org.panda.code.uitl;

/**
 * @Description 数组排序工具类
 * @Author qi
 **/
public class SortUtil {

    /**
     * 冒泡排序
     * @param arr
     * @return
     */
    public static int[] bubblingSort(int[] arr){
        int length = arr.length;
        for (int i=0;i<length;i++){
            for (int j=0;j<length-i-1;j++){
                if (arr[j]>arr[j+1]) {
                    int temp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr;
    }

    /**
     *  选择排序
     * @param arr
     * @return
     */
    public static int[] selectSort(int[] arr){
        int length = arr.length;
        for (int i=0;i<length;i++){
            int min = arr[i];
            int index = i;
            for (int j=i+1;j<length;j++){
                if (min>arr[j]){
                    min = arr[j];
                    index = j;
                }
            }
            int temp = arr[i];
            arr[i] = min;
            arr[index] = temp;
        }
        return arr;
    }

    /**
     * 插入排序
     * @param arr
     * @return
     */
    public static int[] insertSort(int[] arr){
        int length = arr.length;
        for (int i=1;i<length;i++){
            for (int j=i;j>0;j--){
                //
                if (arr[j]<arr[j-1]) {
                    int temp = arr[j - 1];
                    arr[j-1] = arr[j];
                    arr[j] = temp;
                }else {
                    break;
                }
            }
        }
        return arr;
    }

    /**
     * 希尔排序（插入排序变种版）
     * @param arr
     * @return
     */
    public static int[] shellSort(int[] arr){
        int length = arr.length;
        for (int i=length/2;i>0;i/=2){
            for (int j=i;j<length;j++){
                for (int k=j;k>0&&k-i>=0;k-=i){
                    if (arr[k]<arr[k-i]){
                        int temp = arr[k-i];
                        arr[k-i] = arr[k];
                        arr[k] = temp;
                    }else {
                        break;
                    }
                }
            }
        }
        return arr;
    }

    /**
     * 快速排序
     * @param arr
     * @return
     */
    public static void quickSort(int[] arr){
        int low = 0;
        int high = arr.length-1;
        if (high-low<1){
            return;
        }
        boolean flag = true;
        int start = low;
        int end = high;
        int midValue = arr[low];
        while (true){
            if (flag){
                if (arr[high]>midValue){
                    high--;
                }else if (arr[high]<midValue){
                    arr[low] = arr[high];
                    low++;
                    flag = false;
                }
            }else {
                if (arr[low]<midValue){
                    low++;
                }else if (arr[low]>midValue){
                    arr[high] = arr[low];
                    high--;
                    flag = true;
                }
            }
            if (low == high){
                arr[low] = midValue;
                break;
            }
        }
    }

    /**
     * 归并排序
     * @param arr
     * @return
     */
    public static int[] mergeSort(int[] arr){

        return arr;
    }



}
