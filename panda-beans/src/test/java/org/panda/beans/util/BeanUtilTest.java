package org.panda.beans.util;

import org.junit.Test;
import org.panda.beans.entity.SysUser;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 10:25 AM 2019/2/28
 */
public class BeanUtilTest {

    @Test
    public void methodTest(){
        SysUser sysUser = new SysUser();
        sysUser.setId(1L);
        sysUser.setName("liangqi");


        Class clazz = sysUser.getClass();
        Method[] methods = clazz.getMethods();
        for (Method method:methods){
            System.out.println(method.getName());
        }
    }


    @Test
    public void test(){

//        SysUser sysUser = BeanUtil.instantiateClass(SysUser.class);
    //    SysUser sysUser = BeanUtil.instantiateClass(SysUser.class,1L,"qi",new Date(),new Date());
     //   System.out.println(sysUser);
     //   System.out.println(sysUser);
     List<Field> fieldList = BeanUtil.getAllField(SysUser.class);
     fieldList.forEach(field -> {
         System.out.println(field.getName());
     });
    }

}