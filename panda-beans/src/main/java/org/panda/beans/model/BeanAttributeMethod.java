package org.panda.beans.model;

import java.lang.reflect.Method;

public class BeanAttributeMethod {

    /**
     * set方法
     */
    private Method setMethod;
    /**
     * get方法
     */
    private Method getMethod;
    /**
     * 属性名称
     */
    private String fieldName;

    public Method getSetMethod() {
        return setMethod;
    }

    public void setSetMethod(Method setMethod) {
        this.setMethod = setMethod;
    }

    public Method getGetMethod() {
        return getMethod;
    }

    public void setGetMethod(Method getMethod) {
        this.getMethod = getMethod;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
}
