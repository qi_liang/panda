package org.panda.beans.util;


import org.panda.beans.model.BeanAttributeMethod;
import org.panda.code.uitl.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;


/**
 * @author qi
 * 实体类工具
 * Created by qi_liang on 2018/1/29.
 */
public class BeanUtil {

    private final static  Logger LOGGER = LoggerFactory.getLogger(BeanUtil.class);
    /**
     * set方法前缀
     */
    public final static String SET_PREFIX = "set";
    /**
     * get方法前缀
     */
    public final static String GET_PREFIX = "get";
    /**
     * is方法前缀
     */
    public final static String IS_PREFIX = "is";
    /**
     *  获取类属性，包括父类属性
     * @param clazz
     */
    public static List<Field> getAllField(Class clazz){

        final List<Field> fieldList = new ArrayList<>();
        Class currentClass = clazz;
        while (currentClass!=null){
            final Field[] fields = currentClass.getDeclaredFields();
            for (final Field field:fields){
                fieldList.add(field);
            }
            currentClass = currentClass.getSuperclass();
        }
        return fieldList;
    }

    /**
     * 获取类属性
     * @param clazz
     * @return
     */
    public static Field[] getField(Class clazz){

        return clazz.getDeclaredFields();
    }

    /**
     *  获取类属性，包括父类属性
     * @param clazz
     */
    public static List<Method> getAllMethods(Class clazz){
        final List<Method> methodList = new ArrayList<>();
        Class currentClass = clazz;
        while (currentClass!=null){
            Method[] methods = currentClass.getMethods();
            for (Method method:methods){
                methodList.add(method);
            }
            currentClass = currentClass.getSuperclass();
        }
        return methodList;
    }

    /**
     * 获取类方法
     * @param clazz
     * @return
     */
    public static Method[] getMethods(Class clazz){

        return clazz.getMethods();
    }

    /**
     *  获取上一级父类的属性
     * @param clazz
     * @return
     */
    public static Field[] getSuperField(Class clazz){

        return clazz.getSuperclass().getDeclaredFields();
    }

    /**
     *  复制属性，不为空的复制，为空忽略
     */
    public static void copyNotNull(Object target,Object source){

        Class sourceClass = source.getClass();
        List<Field> sourceFieldList  = getAllField(sourceClass);
        Map<String,Field> fieldMap = new HashMap<>(sourceFieldList.size());
        sourceFieldList.forEach(field -> {
            field.setAccessible(true);
            fieldMap.put(field.getName(),field);
        });
        Class targetClass = target.getClass();
        List<Field> targetFieldList = getAllField(targetClass);
        for (Field field:targetFieldList){
            field.setAccessible(true);
            Field targetField = fieldMap.get(field.getName());
            //判断类型是否相等
            if (targetField!=null&&targetField.getGenericType().toString().equals(field.getGenericType().toString())){
                try {
                    PropertyDescriptor pd = new PropertyDescriptor(targetField.getName(), targetClass);
                    //获得写方法
                    Method setFieldMethod = pd.getWriteMethod();
                    Object value =  targetField.get(source);
                    if (value!=null) {
                        setFieldMethod.invoke(target, value);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (IntrospectionException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     *  通过反射获取属性的数据类型
     *  key 属性名
     *  value  类型
     * @param fields
     * @return
     */
    public static Map<String,String> getFiledType(Field[] fields){
        Map<String,String> map=new HashMap(fields.length);
        for (Field field:fields){
            map.put(field.getName(),field.getGenericType().toString());
        }
        return map;
    }

    /**
     *  获取对象属性详情
     * @param fields
     * @return
     */
    public static List<Map<String,String>> getFieldInfo(Field[] fields){
        List<Map<String,String>> list=new ArrayList<>();
        for (Field field:fields){
            Map<String,String> map = new HashMap(2);
            map.put("fieldName",field.getName());
            map.put("fieldType",field.getGenericType().toString());
            list.add(map);
        }
        return list;
    }

    /**
     *  返回属性名列表
     * @param fields
     * @return
     */
    public static List<String> getFiledName(Field[] fields){
        List<String> filedNameList=new ArrayList<>();
        for (Field field:fields){
            filedNameList.add(field.getName());
        }
        return filedNameList;
    }

    /**
     *  返回属性类型列表
     * @param fields
     * @return
     */
    public static List<String> getFileTypeList(Field[] fields){
        List<String> fileTypeList=new ArrayList<>();
        for (Field field:fields){
            fileTypeList.add(field.getGenericType().toString());
        }
        return fileTypeList;
    }


    /**
     * 获取实体名称
     * @param entityClazz
     * @return
     */
    public static String getEntityName(Class entityClazz){
        String entityPath=entityClazz.getName();
        String [] entityNameArray=entityPath.split("\\.");
        String entityName=entityNameArray[entityNameArray.length-1];
        return entityName;
    }

    /**
     *  判断对象是否该与类型一致
     * @param obj
     * @param clazzType
     * @return
     */
    public static Boolean isType(Object obj,Class clazzType){
        boolean isType = false;
        if (obj!=null) {
            Class clazz = obj.getClass();
            isType = clazz.getTypeName().equals(clazzType.getTypeName()) ? true : false;
        }
        return isType;
    }

    /**
     *  获取属性的value
     * @return
     */
    public static Map<String,Object> getFieldValue(Object obj){
        Class clazz = obj.getClass();
        List<Field> fieldList = BeanUtil.getAllField(clazz);
        Map<String,Object> fieldObjectMap = new HashMap<>(fieldList.size());
        fieldList.forEach((field)->{
            try {
                //开启强制破解
                field.setAccessible(true);
                Object value = field.get(obj);
                if (value!=null) {
                    fieldObjectMap.put(field.getName(), obj);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
        return fieldObjectMap;
    }

    /**
     *  通过无参数构造函数返回实例
     * @param clazz
     * @param <T>
     * @return
     */
    public static  <T> T instantiateClass(Class<T> clazz){
        T t = null;
        try {
            t = clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return t;
    }

    /**
     *  通过有参数构造函数返回实例
     * @param clazz
     * @param params
     * @param <T>
     * @return
     */
    public static  <T> T instantiateClass(Class<T> clazz,Object... params){
        T t = null;
        Class[] classes = new Class[params.length];
        for (int i=0;i<params.length;i++){
            classes[i] = params[i].getClass();
        }
        try {
          Constructor constructor = clazz.getConstructor(classes);
          t =(T)constructor.newInstance(params);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return t;
    }

    /**
     * 获取对象的方法
     * @param clazz
     * @param fieldName
     * @return
     */
    public static BeanAttributeMethod getAttributeMethod(Class clazz,String fieldName){
        Method setMethod = null;
        Method getMethod = null;
        BeanAttributeMethod beanAttributeMethod = null;

        try {
            Field field = clazz.getDeclaredField(fieldName);
            String methodEnd = StringUtil.captureName(fieldName);
            setMethod = clazz.getDeclaredMethod(SET_PREFIX+methodEnd,new Class[]{field.getType()});
            getMethod = clazz.getDeclaredMethod(GET_PREFIX+methodEnd,new Class[]{});
            beanAttributeMethod = new BeanAttributeMethod(setMethod,getMethod,fieldName);
        } catch (NoSuchFieldException e) {
            LOGGER.error("无对应属性",e);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return beanAttributeMethod;
    }

    public static void setMethod(Object obj,String fieldName,Object value){
        Class clazz = obj.getClass();
        BeanAttributeMethod beanAttributeMethod = getAttributeMethod(clazz,fieldName);
        Method setMethod = beanAttributeMethod.getSetMethod();

        try {
            setMethod.invoke(obj,new Object[]{value});
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static Object getMethod(Object obj,String fieldName){
        Class clazz = obj.getClass();
        BeanAttributeMethod beanAttributeMethod = getAttributeMethod(clazz,fieldName);
        Method getMethod = beanAttributeMethod.getGetMethod();
        Object[] args;
        Object value = null;
        try {
            value = getMethod.invoke(obj, new Object(){});
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return value;
    }



}
