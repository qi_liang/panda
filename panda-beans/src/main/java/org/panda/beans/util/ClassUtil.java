package org.panda.beans.util;


import org.panda.code.uitl.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author qi
 *  类加载器
 */
public class ClassUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClassUtil.class);

    /**
     *  类加载器
     * @return
     */
    public static ClassLoader getClassLoader(){

        return Thread.currentThread().getContextClassLoader();
    }

    /**
     *  加载类
     * @param className
     * @param isInitialized
     * @return
     */
    public static  Class<?> loadClass(String className,boolean isInitialized){
        Class<?> clazz = null;
        try {
            clazz = Class.forName(className,isInitialized,getClassLoader());
        } catch (ClassNotFoundException e) {
            LOGGER.error("load class failure",e);
            throw new RuntimeException(e);
        }
        return clazz;
    }

    /**
     * 返回类实现接口集合
     */
    public static List<Class> getInterClass(Class clazz){
        Class[] classes = clazz.getInterfaces();
        List<Class> classList = Arrays.asList(classes);
        return classList;
    }

    /**
     *  获取指定包名下的所有类
     * @param packageName
     * @return
     */
    public static Set<Class> getClassSet(String packageName){
        //class 集合
        Set<Class> classSet = new HashSet<>();

        try {
            Enumeration<URL> urls =  getClassLoader().getResources(packageName.replace(".","/"));
            while (urls.hasMoreElements()){
                URL url = urls.nextElement();
                if (url!=null){
                    String protocol = url.getProtocol();
                    if ("file".equals(protocol)){
                        //替换空格
                        String packagePath = url.getPath().replace("%20","");
                        addClass(classSet,packagePath,packageName);
                    }else if ("".equals(protocol)){
                        JarURLConnection jarUrlConnection =(JarURLConnection) url.openConnection();
                        if (jarUrlConnection!=null){
                            JarFile jarFile = jarUrlConnection.getJarFile();
                            if (jarFile!=null){
                                Enumeration<JarEntry> jarEntries = jarFile.entries();
                                while (jarEntries.hasMoreElements()){
                                    JarEntry jarEntry = jarEntries.nextElement();
                                    String jarEntryName = jarEntry.getName();
                                    if (jarEntryName.endsWith(".class")){
                                        String className = jarEntryName.substring(0,jarEntryName.lastIndexOf("."))
                                                .replace("/",".");
                                        doAddClass(classSet,className);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            LOGGER.error("获取实体类流异常",e);
            throw new RuntimeException(e);
        }
        return classSet;
    }

    /**
     *  该处可以使用jdk8新特性优化
     * @param classSet
     * @param packagePath
     * @param packageName
     */
    private static void addClass(Set<Class> classSet,String packagePath,String packageName){
        File[] files = new File(packagePath).listFiles((File file)->{
            return (file.isFile()&&file.getName().endsWith(".class"))||file.isDirectory();
        });
        for (File file:files){
            String fileName = file.getName();
            if (file.isFile()){
                String className = fileName.substring(0,fileName.lastIndexOf("."));
                if (StringUtil.isNotNull(packageName)){
                    className = packageName+"."+className;
                    doAddClass(classSet,className);
                }
            }else {
                String subPackagePath = fileName;
                if (StringUtil.isNotNull(subPackagePath)){
                    subPackagePath = packagePath+"/"+subPackagePath;
                }
                String subPackageName = fileName;
                if (StringUtil.isNotNull(subPackageName)){
                    subPackageName = packageName+"."+subPackageName;
                }
                addClass(classSet,subPackagePath,subPackageName);
            }
        }

    }

    /**
     * 通过类名，添加到类集合中
     * @param classSet 类集合
     * @param className 类名
     */
    public static void doAddClass(Set<Class> classSet,String className){
        Class<?> clazz = loadClass(className,false);
        classSet.add(clazz);
    }


}
