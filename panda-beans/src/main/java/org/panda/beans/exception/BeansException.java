package org.panda.beans.exception;

/**
 *  bean 自定义异常
 * @author qi
 */
public class BeansException extends Exception{

    public BeansException() {
    }

    public BeansException(String msg,int value){
        super(msg);
        this.value = value;
    }

    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
