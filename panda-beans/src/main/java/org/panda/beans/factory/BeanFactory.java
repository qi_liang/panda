package org.panda.beans.factory;

/**
 * @Author: qi
 * @Description:实例工厂
 * @Date: Create in 9:06 AM 2019/1/29
 *
 */
public interface BeanFactory {

    /**
     * 实例 -> 工厂转义符
     */
    String FACTORY_BEAN_PREFIX = "&";

    /**
     *  通过类名查找实例
     * @param name
     * @return
     */
    Object getBean(String name);

    /**
     *  通过类名查找实例并确定类型
     * @param name
     * @param requiredType
     * @param <T>
     * @return
     */
    <T> T getBean(String name,Class<T> requiredType);

    /**
     * 通过类名查找实例
     * @param name
     * @param args
     * @return
     */
    Object getBean(String name,Object... args);

    /**
     * 通过class查找实例
     * @param requiredType
     * @param <T>
     * @return
     */
    <T> T getBean(Class<T> requiredType);


    /**
     * 通过实例名,返回实例class
     * @param name
     * @return
     */
    Class<?> getType(String name);

    /**
     *  通过实例名返回该实例的别名数组
     * @param name
     * @return
     */
    String[] getAliases(String name);

    /**
     *  通过实例名判断实例是否存在
     * @param name
     * @return  存在：true  不存在：false
     */
    boolean containsBean(String name);

    /**
     *  singletion和protpType区别
     *  1.singletion在容器中只有一个实例,protoType则是每次getBean的时候创建一个实例
     */

    /**
     * 通过实例名判断实例是否是单例
     * @param name
     * @return 是:true 否：false
     */
    boolean isSingletion(String name);

    /**
     *  通过实例名判断实例是否原型
     * @param name
     * @return 是:true 否：false
     */
    boolean isProtoType(String name);

    /**
     *  检查实例是否与class类型是否匹配
     * @param name
     * @param typeToMatch
     * @return  是:true 否:false
     */
    boolean isTypeMatch(String name,Class<?> typeToMatch);
}
