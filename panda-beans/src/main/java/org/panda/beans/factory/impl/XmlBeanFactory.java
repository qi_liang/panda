package org.panda.beans.factory.impl;

import org.panda.beans.factory.BeanFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description xml工厂类
 * @Author qi
 **/
public class XmlBeanFactory implements BeanFactory {

    Map<String,Object> beanMap = new HashMap<>();

    public XmlBeanFactory(String resources){


    }

    @Override
    public Object getBean(String name) {
        return beanMap.get(name);
    }

    @Override
    public <T> T getBean(String name, Class<T> requiredType) {
        Object bean = beanMap.get(name);
        if (bean!=null) {
          T typeBean = requiredType.cast(bean);
          return typeBean;
        }
        return null;
    }

    @Override
    public Object getBean(String name, Object... args) {
        return null;
    }

    @Override
    public <T> T getBean(Class<T> requiredType) {
        return null;
    }

    @Override
    public Class<?> getType(String name) {
        return null;
    }

    @Override
    public String[] getAliases(String name) {
        return new String[0];
    }

    @Override
    public boolean containsBean(String name) {
        return false;
    }

    @Override
    public boolean isSingletion(String name) {
        return false;
    }

    @Override
    public boolean isProtoType(String name) {
        return false;
    }

    @Override
    public boolean isTypeMatch(String name, Class<?> typeToMatch) {
        return false;
    }
}
